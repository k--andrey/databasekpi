<?php
	function http_digest_parse($txt) {
		// protect against missing data
		$needed_parts = array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 'username'=>1, 'uri'=>1, 'response'=>1);
		$data = array();
		$keys = implode('|', array_keys($needed_parts));
		preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);
		foreach ($matches as $m) {
			$data[$m[1]] = $m[3] ? $m[3] : $m[4];
			unset($needed_parts[$m[1]]);
		}
		return $needed_parts ? false : $data;
	}
	if(isset($_REQUEST["logout"])){
		$_SERVER['PHP_AUTH_DIGEST']=null;
		$_SESSION["SID"]=null;
	}
	require('conndb.php');
	$realm = 'mobilnist db';//ALL PASSWORD WILL BE RESET ON CHANGE. DUPLICATE ALL CHANGES in user_insert.php
	$sendAuthHead = false;
	$ErrPage = '';
	//user => password
	$query = sprintf('select u_name, u_pass from user_list');
	$result = mysql_query($query) or die(mysql_error());
	$users = array();
	while ($row = mysql_fetch_assoc($result)){
		$users[$row['u_name']] = $row['u_pass'];
	}
	if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
		$sendAuthHead = true;
		$ErrPage = 'Cancel Pressed';
	} else {
		// analyze the PHP_AUTH_DIGEST variable
		if (!($data = http_digest_parse($_SERVER['PHP_AUTH_DIGEST'])) ||
				!isset($users[$data['username']])){
			$sendAuthHead = true;
			$ErrPage = 'Wrong Credentials1!Cancel Pressed';
		} else {
			// generate the valid response
			//A1 = md5($data['username'] . ':' . $realm . ':' . $users[$data['username']]);
			$A1 = $users[$data['username']];
			$A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
			$valid_response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);
			if ($data['response'] != $valid_response){
				$sendAuthHead = true;
				$ErrPage = 'Wrong Credentials2!Cancel Pressed';
			}
		}
	}
	if ($sendAuthHead){
		header('HTTP/1.1 401 Unauthorized');
		header('WWW-Authenticate: Digest realm="'.$realm.
'",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');
		die($ErrPage);
	} else {
		session_start();
		$SID = htmlspecialchars($_SERVER['REMOTE_ADDR'].$_SERVER["HTTP_USER_AGENT"]);
		$_SESSION['SID'] = md5($SID);
		$_SESSION['U_NAME']=$data['username'];
		mysql_query(sprintf("insert into log (u_name, l_info) values ('%s','user logon')",$_SESSION['U_NAME'])); 
		if(mysql_result(mysql_query(sprintf("select u_type from user_list where u_name = '%s'",$data['username'])),0)) {
			header('Location: deep.php');
		} else {
			header("Location: main.php");
		}			
 	}
?>
