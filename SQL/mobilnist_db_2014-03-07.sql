-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Мар 07 2014 г., 13:28
-- Версия сервера: 5.5.25
-- Версия PHP: 5.2.12

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `mobilnist_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cafedra`
--

CREATE TABLE IF NOT EXISTS `cafedra` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=125 ;

--
-- Дамп данных таблицы `cafedra`
--

INSERT INTO `cafedra` (`id`, `name`) VALUES
(1, 'Кафедра телекомунікацій'),
(2, 'Кафедра телекомунікаційних систем '),
(3, 'Кафедра інформаційно телекомунікаційних мереж'),
(4, 'Кафедра машин і агрегатiв полiграфiчного виробництва '),
(5, 'Кафедра технологiї полiграфiчного виробництва'),
(6, 'Кафедра видавничої справи і редагування'),
(7, 'кафедра органiзацiї видавничої cправи, полiграфiї та книгорозповсюдження - See more at: http://kpi.u'),
(8, 'Кафедра органiзацiї видавничої cправи, полiграфiї та книгорозповсюдження '),
(9, 'Кафедра графiки'),
(10, 'Кафедра репрографії'),
(11, 'Кафедра зварювального виробництва'),
(12, 'Кафедра електрозварювальних установок'),
(13, 'Кафедра інженерії поверхні'),
(14, 'Кафедра електропостачання'),
(15, 'Кафедра автоматизації управління електротехнічними комплексами'),
(16, 'Кафедра електромеханічного обладнання енергоємних виробництв'),
(17, 'Кафедра теплотехніки та енергозбереження'),
(18, 'Кафедра інженерної екології'),
(19, 'Кафедра геобудівництва та гірничих технологій'),
(20, 'Кафедра охорони праці, промислової та цивільної безпеки'),
(21, 'Кафедра математичних методів системного аналізу'),
(22, 'Кафедра системного проектування'),
(23, 'Кафедра ливарного виробництва чорних i кольорових металiв '),
(24, 'Кафедра металознавства та термiчної обробки'),
(25, 'Кафедра високотемпературних матерiалiв та порошкової металургiї '),
(26, 'Кафедра фiзико-хiмiчних основ технологiї металiв'),
(27, 'Кафедра фiзики металiв'),
(28, 'Кафедра хiмiчного, полiмерного і силікатного машинобудування'),
(29, 'Кафедра автоматизацiї хiмiчних виробництв'),
(30, 'Кафедра машин і апаратiв хiмiчних та нафтопереробних виробництв'),
(31, 'Кафедра екології та технології рослинних полімерів'),
(32, 'Кафедра динаміки і мiцностi машин та опору матерiалiв '),
(33, 'Кафедра інтегрованих технологій машинобудування'),
(34, 'Кафедра технології машинобудування'),
(35, 'Кафедра механіки пластичності матеріалів та ресурсозберігаючих процесів'),
(36, 'Кафедра конструювання верстатів i машин'),
(37, 'Кафедра прикладної гідроаеромеханіки і механотроніки'),
(38, 'Кафедра прикладної механіки'),
(39, 'Кафедра лазерної техніки та фізико-технічних технологій'),
(40, 'Кафедра приладобудування'),
(41, 'Кафедра виробництва приладів'),
(42, 'Кафедра приладів і систем орієнтації та навігації'),
(43, 'Кафедра оптичних та оптико-електронних приладiв'),
(44, 'Кафедра приладiв i систем неруйнівного контролю'),
(45, 'Кафедра наукових, аналiтичних та екологiчних приладiв i систем '),
(46, 'Кафедра теоретичних основ радіотехніки'),
(47, 'Кафедра радіоприймання та оброблення сигналів'),
(48, 'Кафедра радіотехнічних пристроїв і систем'),
(49, 'Кафедра радіоконструювання і виробництва радіоелектронної апаратури '),
(50, 'Кафедра автоматизації проектування енергетичних процесів та систем '),
(51, 'Кафедра атомних електричних станцій та інженерної теплофізики '),
(52, 'Кафедра автоматизації теплоенергетичних процесів'),
(53, 'Кафедра теоретичної та промислової теплотехніки'),
(54, 'Кафедра теплоенергетичних установок теплових та атомних електростанцій'),
(55, 'Кафедра приладів та систем керування літальними апаратами '),
(56, 'Кафедра автоматизації експериментальних дослiджень'),
(57, 'Кафедра інформацiйно-вимiрювальної техніки'),
(58, 'Кафедра теоретичної механіки'),
(59, 'Кафедра біомедичної кібернетики'),
(60, 'Кафедра біомедичної інженерії'),
(61, 'Кафедра біобезпеки і відновної біоінженерії'),
(62, 'Кафедра фізичної реабілітації'),
(63, 'Кафедра фізичного виховання'),
(64, 'Кафедра спортивного вдосконалення'),
(65, 'Кафедра промислової біотехнології'),
(66, 'Кафедра екобіотехнології та біоенергетики'),
(67, 'Кафедра біотехніки та інженерії'),
(68, 'Кафедра біоінформатики'),
(69, 'Кафедра електромеханіки'),
(70, 'Кафедра електричних станцій'),
(71, 'Кафедра автоматизації енергосистем'),
(72, 'Кафедра автоматизації електромеханічних систем та електроприводу '),
(73, 'Кафедра електричних мереж і систем'),
(74, 'Кафедра техніки і електрофізики високих напруг'),
(75, 'Кафедра відновлювальних джерел енергії'),
(76, 'Кафедра теоретичної електротехніки'),
(77, 'Кафедра мікроелектроніки'),
(78, 'Кафедра електронних приладів та пристроїв'),
(79, 'Кафедра промислової електроніки'),
(80, 'Кафедра фізичної та біомедичної електроніки'),
(81, 'Кафедра конструювання електронно-обчислювальної апаратури'),
(82, 'Кафедра акустики і акустоелектроніки'),
(83, 'Кафедра звукотехніки і реєстрації інформацiї'),
(84, 'Кафедра автоматики i управлiння в технiчних системах'),
(85, 'Кафедра обчислювальної технiки'),
(86, 'Кафедра технiчної кiбернетики'),
(87, 'Кафедра автоматизованих систем обробки iнформацiї та управлiння'),
(88, 'Кафедра теорії, практики та перекладу англійської мови'),
(89, 'Кафедра теорії, практики та перекладу французької мови'),
(90, 'Кафедра теорії, практики та перекладу німецької мови '),
(91, 'Кафедра менеджменту'),
(92, 'Кафедра промислового маркетингу'),
(93, 'Кафедра математичного моделювання економічних систем '),
(94, 'Кафедра економіки і підприємництва'),
(95, 'Кафедра міжнародної економіки'),
(96, 'Кафедра математичного аналiзу та теорiї ймовiрностей '),
(97, 'Кафедра диференціальних рiвнянь'),
(98, 'Кафедра математичної фiзики'),
(99, 'Кафедра загальної фiзики та фiзики твердого тiла'),
(100, 'Кафедра загальної та експериментальної фiзики'),
(101, 'Кафедра загальної та теоретичної фiзики'),
(102, 'Кафедра нарисної геометрiї, iнженерної та комп\\''ютерної графiки'),
(103, 'Кафедра прикладної математики'),
(104, 'Кафедра системного програмування і спеціалізованих комп’ютерних систем '),
(105, 'Кафедра програмного забезпечення комп`ютерних систем'),
(106, 'Кафедра соціології'),
(108, 'Кафедра теорії та практики управління'),
(109, 'Кафедра публічного права'),
(110, 'Кафедра господарського та адміністративного права'),
(111, 'Кафедра інформаційного права та права інтелектуальної власності'),
(112, 'Кафедра прикладної фізики'),
(113, 'Кафедра інформаційної безпеки'),
(114, 'Кафедра математичних методів захисту інформації'),
(115, 'Кафедра фізико-технічних засобів захисту інформації '),
(116, 'Кафедра технології неорганічних речовин та загальної хімічної технології'),
(117, 'Кафедра органічної хімії та технології органічних речовин'),
(118, 'Кафедра технології електрохімічних виробництв'),
(119, 'Кафедра загальної та неорганічної хімії'),
(121, 'Кафедра фізичної хімії'),
(122, 'Кафедра хімічної технології композиційних матеріалів'),
(123, 'Кафедра хімічної технології кераміки та скла'),
(124, 'Кафедра кібернетики хіміко-технологічних процесів');

-- --------------------------------------------------------

--
-- Структура таблицы `faculty`
--

CREATE TABLE IF NOT EXISTS `faculty` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Дамп данных таблицы `faculty`
--

INSERT INTO `faculty` (`id`, `name`) VALUES
(3, 'ВПІ'),
(22, 'ЗФ'),
(10, 'ІЕЕ'),
(7, 'ІПСА'),
(2, 'ІТС'),
(11, 'ІФФ'),
(23, 'ІХФ'),
(21, 'ММІ'),
(12, 'ПБФ'),
(15, 'РТФ'),
(25, 'ТЕФ'),
(4, 'ФАКС'),
(18, 'ФБМІ'),
(19, 'ФБТ'),
(20, 'ФЕА'),
(13, 'ФЕЛ'),
(1, 'ФІОТ'),
(5, 'ФЛ'),
(8, 'ФММ'),
(14, 'ФМФ'),
(9, 'ФПМ'),
(16, 'ФСП'),
(24, 'ФТІ'),
(6, 'ХТФ');

-- --------------------------------------------------------

--
-- Структура таблицы `financingsource`
--

CREATE TABLE IF NOT EXISTS `financingsource` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `source` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `source` (`source`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `financingsource`
--

INSERT INTO `financingsource` (`id`, `source`) VALUES
(1, 'за власний рахунок'),
(3, 'за рахунок державних коштів'),
(2, 'за рахунок приймаючої сторони');

-- --------------------------------------------------------

--
-- Структура таблицы `grupa`
--

CREATE TABLE IF NOT EXISTS `grupa` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `faculty` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `faculty` (`faculty`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=37 ;

--
-- Дамп данных таблицы `grupa`
--

INSERT INTO `grupa` (`id`, `name`, `faculty`) VALUES
(1, 'СМ-31с', 3),
(2, 'СТ-31с', 3),
(3, 'РП-31с', 3),
(4, 'МВ-31с', 3),
(5, 'ТМ-31с', 3),
(6, 'СР-31с', 3),
(7, 'СГ-31с', 3),
(8, 'СМ-31м', 3),
(9, 'СТ-31м', 3),
(10, 'РП-31м', 3),
(11, 'МВ-31м', 3),
(12, 'ТМ-31м', 3),
(13, 'СЕ-31м', 3),
(14, 'СК-31м', 3),
(15, 'СР-31м', 3),
(16, 'РЗ-31м', 3),
(17, 'СГ-31м', 3),
(18, 'СР-81м', 3),
(19, 'ДП-31м', 13),
(20, 'КВ-84м', 9),
(21, 'ЛА-31м', 23),
(22, 'ПІ-81м', 12),
(23, 'ДЕ-31м', 13),
(24, 'ІО-81м', 1),
(25, 'УВ-31м', 8),
(26, 'ТЗ-81м', 2),
(27, 'ТС-81м', 2),
(28, 'ТС-83м', 2),
(29, 'КВ-83м', 9),
(30, 'КВ-81м', 9),
(31, 'ХП-81м', 6),
(32, 'ХО-81м', 6),
(33, 'ТК-81м', 25),
(34, 'ХЕ-31м', 6),
(35, 'ХО-31м', 6);

-- --------------------------------------------------------

--
-- Структура таблицы `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `l_info` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `u_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `l_tstamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `log`
--

INSERT INTO `log` (`l_info`, `u_name`, `l_tstamp`) VALUES
('user logon', 'super', '2014-02-28 10:37:46'),
('added user olena with TYPE 0', 'super', '2014-02-28 10:40:23'),
('user logon', 'olena', '2014-02-28 10:40:56'),
('user logon', 'olena', '2014-02-28 10:50:26'),
('INSERT INTO faculty (name) values (''ФІОТ'')', 'olena', '2014-02-28 10:51:46'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Владислав'',''Володимирович'',''Медведєв'','''','''')', 'olena', '2014-02-28 10:55:08'),
('UPDATE student SET name=''Владислав'', middlename = ''Володимирович'', lastname = ''Медведєв'', contacts = '''' WHERE id = ''1''', 'olena', '2014-02-28 10:56:23'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Ганна'',''Євгенівна'',''Кібякова'','''','''')', 'olena', '2014-02-28 10:57:35'),
('INSERT INTO university (name, city, country) VALUES (''Університетський коледж Телемарк'', ''Порсгрунн'', ''Норвегія'')', 'olena', '2014-02-28 10:58:53'),
('INSERT INTO faculty (name) values (''ІТС'')', 'olena', '2014-02-28 14:08:27'),
('INSERT INTO faculty (name) values (''ВПІ'')', 'olena', '2014-02-28 14:08:36'),
('INSERT INTO faculty (name) values (''ФАКС'')', 'olena', '2014-02-28 14:08:49'),
('INSERT INTO faculty (name) values (''ФЛ'')', 'olena', '2014-02-28 14:08:57'),
('INSERT INTO faculty (name) values (''ХТФ'')', 'olena', '2014-02-28 14:09:01'),
('INSERT INTO faculty (name) values (''ІПСА'')', 'olena', '2014-02-28 14:09:09'),
('INSERT INTO faculty (name) values (''ФММ'')', 'olena', '2014-02-28 14:09:14'),
('INSERT INTO faculty (name) values (''ФПМ'')', 'olena', '2014-02-28 14:09:19'),
('INSERT INTO faculty (name) values (''ІЕЕ'')', 'olena', '2014-02-28 14:09:26'),
('INSERT INTO faculty (name) values (''ІФФ'')', 'olena', '2014-02-28 14:09:33'),
('INSERT INTO faculty (name) values (''ПБФ'')', 'olena', '2014-02-28 14:09:38'),
('INSERT INTO faculty (name) values (''ФЕЛ'')', 'olena', '2014-02-28 14:09:44'),
('INSERT INTO faculty (name) values (''ФМФ'')', 'olena', '2014-02-28 14:09:51'),
('INSERT INTO faculty (name) values (''РТФ'')', 'olena', '2014-02-28 14:09:54'),
('INSERT INTO faculty (name) values (''ФСП'')', 'olena', '2014-02-28 14:10:00'),
('INSERT INTO faculty (name) values (''ММІФ'')', 'olena', '2014-02-28 14:10:07'),
('DELETE FROM faculty WHERE id = 17', 'olena', '2014-02-28 14:10:15'),
('INSERT INTO faculty (name) values (''ФБМІ'')', 'olena', '2014-02-28 14:10:20'),
('INSERT INTO faculty (name) values (''ФБТ'')', 'olena', '2014-02-28 14:10:30'),
('INSERT INTO faculty (name) values (''ФЕА'')', 'olena', '2014-02-28 14:10:37'),
('INSERT INTO faculty (name) values (''ММІ'')', 'olena', '2014-02-28 14:10:44'),
('INSERT INTO faculty (name) values (''ЗФ'')', 'olena', '2014-02-28 14:10:53'),
('INSERT INTO faculty (name) values (''ІХФ'')', 'olena', '2014-02-28 14:10:58'),
('INSERT INTO faculty (name) values (''ФТІ'')', 'olena', '2014-02-28 14:11:03'),
('INSERT INTO faculty (name) values (''ТЕФ'')', 'olena', '2014-02-28 14:12:21'),
('INSERT INTO cafedra (name) VALUES (''Кафедра телекомунікацій'')', 'olena', '2014-02-28 14:14:41'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра телекомунікацій''), ''2'')', 'olena', '2014-02-28 14:14:41'),
('INSERT INTO cafedra (name) VALUES (''Кафедра телекомунікаційних систем '')', 'olena', '2014-02-28 14:15:37'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра телекомунікаційних систем ''), ''2'')', 'olena', '2014-02-28 14:15:37'),
('INSERT INTO cafedra (name) VALUES (''Кафедра інформаційно телекомунікаційних мереж'')', 'olena', '2014-02-28 14:16:34'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра інформаційно телекомунікаційних мереж''), ''2'')', 'olena', '2014-02-28 14:16:35'),
('INSERT INTO cafedra (name) VALUES (''Кафедра машин і агрегатiв полiграфiчного виробництва '')', 'olena', '2014-02-28 14:18:02'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра машин і агрегатiв полiграфiчного виробництва ''), ''3'')', 'olena', '2014-02-28 14:18:03'),
('INSERT INTO cafedra (name) VALUES (''Кафедра технологiї полiграфiчного виробництва'')', 'olena', '2014-02-28 14:18:25'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра технологiї полiграфiчного виробництва''), ''3'')', 'olena', '2014-02-28 14:18:25'),
('INSERT INTO cafedra (name) VALUES (''Кафедра видавничої справи і редагування'')', 'olena', '2014-02-28 14:18:46'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра видавничої справи і редагування''), ''3'')', 'olena', '2014-02-28 14:18:46'),
('INSERT INTO cafedra (name) VALUES (''кафедра органiзацiї видавничої cправи, полiграфiї та книгорозповсюдження - See more at: http://kpi.ua/vpi#sthash.KkC57D2F.dpuf'')', 'olena', '2014-02-28 14:19:14'),
('INSERT INTO cafedra (name) VALUES (''Кафедра органiзацiї видавничої cправи, полiграфiї та книгорозповсюдження '')', 'olena', '2014-02-28 14:19:30'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра органiзацiї видавничої cправи, полiграфiї та книгорозповсюдження ''), ''3'')', 'olena', '2014-02-28 14:19:30'),
('INSERT INTO cafedra (name) VALUES (''Кафедра графiки'')', 'olena', '2014-02-28 14:19:56'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра графiки''), ''3'')', 'olena', '2014-02-28 14:19:56'),
('INSERT INTO cafedra (name) VALUES (''Кафедра репрографії'')', 'olena', '2014-02-28 14:20:14'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра репрографії''), ''3'')', 'olena', '2014-02-28 14:20:14'),
('INSERT INTO cafedra (name) VALUES (''Кафедра зварювального виробництва'')', 'olena', '2014-02-28 14:20:50'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра зварювального виробництва''), ''22'')', 'olena', '2014-02-28 14:20:50'),
('INSERT INTO cafedra (name) VALUES (''Кафедра електрозварювальних установок'')', 'olena', '2014-02-28 14:21:04'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра електрозварювальних установок''), ''22'')', 'olena', '2014-02-28 14:21:04'),
('INSERT INTO cafedra (name) VALUES (''Кафедра інженерії поверхні'')', 'olena', '2014-02-28 14:21:26'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра інженерії поверхні''), ''22'')', 'olena', '2014-02-28 14:21:26'),
('INSERT INTO cafedra (name) VALUES (''Кафедра електропостачання'')', 'olena', '2014-02-28 14:22:47'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра електропостачання''), ''10'')', 'olena', '2014-02-28 14:22:47'),
('INSERT INTO cafedra (name) VALUES (''Кафедра автоматизації управління електротехнічними комплексами'')', 'olena', '2014-02-28 14:23:29'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра автоматизації управління електротехнічними комплексами''), ''10'')', 'olena', '2014-02-28 14:23:29'),
('INSERT INTO cafedra (name) VALUES (''Кафедра електромеханічного обладнання енергоємних виробництв'')', 'olena', '2014-02-28 14:24:09'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра електромеханічного обладнання енергоємних виробництв''), ''10'')', 'olena', '2014-02-28 14:24:09'),
('INSERT INTO cafedra (name) VALUES (''Кафедра теплотехніки та енергозбереження'')', 'olena', '2014-02-28 14:24:42'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра теплотехніки та енергозбереження''), ''10'')', 'olena', '2014-02-28 14:24:42'),
('INSERT INTO cafedra (name) VALUES (''Кафедра інженерної екології'')', 'olena', '2014-02-28 14:25:03'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра інженерної екології''), ''10'')', 'olena', '2014-02-28 14:25:03'),
('INSERT INTO cafedra (name) VALUES (''Кафедра геобудівництва та гірничих технологій'')', 'olena', '2014-02-28 14:25:31'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра геобудівництва та гірничих технологій''), ''10'')', 'olena', '2014-02-28 14:25:31'),
('INSERT INTO cafedra (name) VALUES (''Кафедра охорони праці, промислової та цивільної безпеки'')', 'olena', '2014-02-28 14:26:03'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра охорони праці, промислової та цивільної безпеки''), ''10'')', 'olena', '2014-02-28 14:26:03'),
('INSERT INTO cafedra (name) VALUES (''Кафедра математичних методів системного аналізу'')', 'olena', '2014-02-28 14:27:11'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра математичних методів системного аналізу''), ''7'')', 'olena', '2014-02-28 14:27:11'),
('INSERT INTO cafedra (name) VALUES (''Кафедра системного проектування'')', 'olena', '2014-02-28 14:27:25'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра системного проектування''), ''7'')', 'olena', '2014-02-28 14:27:25'),
('INSERT INTO cafedra (name) VALUES (''Кафедра ливарного виробництва чорних i кольорових металiв '')', 'olena', '2014-02-28 14:28:27'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра ливарного виробництва чорних i кольорових металiв ''), ''11'')', 'olena', '2014-02-28 14:28:28'),
('INSERT INTO cafedra (name) VALUES (''Кафедра металознавства та термiчної обробки'')', 'olena', '2014-02-28 14:28:53'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра металознавства та термiчної обробки''), ''11'')', 'olena', '2014-02-28 14:28:53'),
('INSERT INTO cafedra (name) VALUES (''Кафедра високотемпературних матерiалiв та порошкової металургiї '')', 'olena', '2014-02-28 14:29:53'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра високотемпературних матерiалiв та порошкової металургiї ''), ''11'')', 'olena', '2014-02-28 14:29:54'),
('INSERT INTO cafedra (name) VALUES (''Кафедра фiзико-хiмiчних основ технологiї металiв'')', 'olena', '2014-02-28 14:30:11'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра фiзико-хiмiчних основ технологiї металiв''), ''11'')', 'olena', '2014-02-28 14:30:11'),
('INSERT INTO cafedra (name) VALUES (''Кафедра фiзики металiв'')', 'olena', '2014-02-28 14:30:30'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра фiзики металiв''), ''11'')', 'olena', '2014-02-28 14:30:30'),
('INSERT INTO cafedra (name) VALUES (''Кафедра хiмiчного, полiмерного і силікатного машинобудування'')', 'olena', '2014-02-28 14:31:08'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра хiмiчного, полiмерного і силікатного машинобудування''), ''23'')', 'olena', '2014-02-28 14:31:09'),
('INSERT INTO cafedra (name) VALUES (''Кафедра автоматизацiї хiмiчних виробництв'')', 'olena', '2014-02-28 14:31:26'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра автоматизацiї хiмiчних виробництв''), ''23'')', 'olena', '2014-02-28 14:31:26'),
('INSERT INTO cafedra (name) VALUES (''Кафедра машин і апаратiв хiмiчних та нафтопереробних виробництв'')', 'olena', '2014-02-28 14:34:03'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра машин і апаратiв хiмiчних та нафтопереробних виробництв''), ''23'')', 'olena', '2014-02-28 14:34:03'),
('INSERT INTO cafedra (name) VALUES (''Кафедра екології та технології рослинних полімерів'')', 'olena', '2014-02-28 14:34:23'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра екології та технології рослинних полімерів''), ''23'')', 'olena', '2014-02-28 14:34:23'),
('INSERT INTO cafedra (name) VALUES (''Кафедра динаміки і мiцностi машин та опору матерiалiв '')', 'olena', '2014-02-28 14:35:28'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра динаміки і мiцностi машин та опору матерiалiв ''), ''21'')', 'olena', '2014-02-28 14:35:28'),
('INSERT INTO cafedra (name) VALUES (''Кафедра інтегрованих технологій машинобудування'')', 'olena', '2014-02-28 14:35:43'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра інтегрованих технологій машинобудування''), ''21'')', 'olena', '2014-02-28 14:35:43'),
('INSERT INTO cafedra (name) VALUES (''Кафедра технології машинобудування'')', 'olena', '2014-02-28 14:35:54'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра технології машинобудування''), ''21'')', 'olena', '2014-02-28 14:35:55'),
('INSERT INTO cafedra (name) VALUES (''Кафедра механіки пластичності матеріалів та ресурсозберігаючих процесів'')', 'olena', '2014-02-28 14:36:26'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра механіки пластичності матеріалів та ресурсозберігаючих процесів''), ''21'')', 'olena', '2014-02-28 14:36:27'),
('INSERT INTO cafedra (name) VALUES (''Кафедра конструювання верстатів i машин'')', 'olena', '2014-02-28 14:36:41'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра конструювання верстатів i машин''), ''21'')', 'olena', '2014-02-28 14:36:41'),
('INSERT INTO cafedra (name) VALUES (''Кафедра прикладної гідроаеромеханіки і механотроніки'')', 'olena', '2014-02-28 14:37:00'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра прикладної гідроаеромеханіки і механотроніки''), ''21'')', 'olena', '2014-02-28 14:37:00'),
('INSERT INTO cafedra (name) VALUES (''Кафедра прикладної механіки'')', 'olena', '2014-02-28 14:37:14'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра прикладної механіки''), ''21'')', 'olena', '2014-02-28 14:37:14'),
('INSERT INTO cafedra (name) VALUES (''Кафедра лазерної техніки та фізико-технічних технологій'')', 'olena', '2014-02-28 14:37:31'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра лазерної техніки та фізико-технічних технологій''), ''21'')', 'olena', '2014-02-28 14:37:31'),
('INSERT INTO cafedra (name) VALUES (''Кафедра приладобудування'')', 'olena', '2014-02-28 14:38:52'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра приладобудування''), ''12'')', 'olena', '2014-02-28 14:38:52'),
('INSERT INTO cafedra (name) VALUES (''Кафедра виробництва приладів'')', 'olena', '2014-02-28 14:39:18'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра виробництва приладів''), ''12'')', 'olena', '2014-02-28 14:39:18'),
('INSERT INTO cafedra (name) VALUES (''Кафедра приладів і систем орієнтації та навігації'')', 'olena', '2014-02-28 14:40:54'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра приладів і систем орієнтації та навігації''), ''12'')', 'olena', '2014-02-28 14:40:54'),
('INSERT INTO cafedra (name) VALUES (''Кафедра оптичних та оптико-електронних приладiв'')', 'olena', '2014-02-28 14:41:14'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра оптичних та оптико-електронних приладiв''), ''12'')', 'olena', '2014-02-28 14:41:14'),
('INSERT INTO cafedra (name) VALUES (''Кафедра приладiв i систем неруйнівного контролю'')', 'olena', '2014-02-28 14:41:36'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра приладiв i систем неруйнівного контролю''), ''12'')', 'olena', '2014-02-28 14:41:36'),
('INSERT INTO cafedra (name) VALUES (''Кафедра наукових, аналiтичних та екологiчних приладiв i систем '')', 'olena', '2014-02-28 14:41:58'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра наукових, аналiтичних та екологiчних приладiв i систем ''), ''12'')', 'olena', '2014-02-28 14:41:58'),
('INSERT INTO cafedra (name) VALUES (''Кафедра теоретичних основ радіотехніки'')', 'olena', '2014-02-28 14:50:45'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра теоретичних основ радіотехніки''), ''15'')', 'olena', '2014-02-28 14:50:45'),
('INSERT INTO cafedra (name) VALUES (''Кафедра радіоприймання та оброблення сигналів'')', 'olena', '2014-02-28 14:51:13'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра радіоприймання та оброблення сигналів''), ''15'')', 'olena', '2014-02-28 14:51:13'),
('INSERT INTO cafedra (name) VALUES (''Кафедра радіотехнічних пристроїв і систем'')', 'olena', '2014-02-28 14:52:14'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра радіотехнічних пристроїв і систем''), ''15'')', 'olena', '2014-02-28 14:52:14'),
('INSERT INTO cafedra (name) VALUES (''Кафедра радіоконструювання і виробництва радіоелектронної апаратури '')', 'olena', '2014-02-28 14:52:46'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра радіоконструювання і виробництва радіоелектронної апаратури ''), ''15'')', 'olena', '2014-02-28 14:52:46'),
('INSERT INTO cafedra (name) VALUES (''Кафедра автоматизації проектування енергетичних процесів та систем '')', 'olena', '2014-02-28 14:54:12'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра автоматизації проектування енергетичних процесів та систем ''), ''25'')', 'olena', '2014-02-28 14:54:13'),
('INSERT INTO cafedra (name) VALUES (''Кафедра атомних електричних станцій та інженерної теплофізики '')', 'olena', '2014-02-28 14:55:04'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра атомних електричних станцій та інженерної теплофізики ''), ''25'')', 'olena', '2014-02-28 14:55:04'),
('INSERT INTO cafedra (name) VALUES (''Кафедра автоматизації теплоенергетичних процесів'')', 'olena', '2014-02-28 14:55:56'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра автоматизації теплоенергетичних процесів''), ''25'')', 'olena', '2014-02-28 14:55:56'),
('INSERT INTO cafedra (name) VALUES (''Кафедра теоретичної та промислової теплотехніки'')', 'olena', '2014-02-28 14:56:18'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра теоретичної та промислової теплотехніки''), ''25'')', 'olena', '2014-02-28 14:56:18'),
('INSERT INTO cafedra (name) VALUES (''Кафедра теплоенергетичних установок теплових та атомних електростанцій'')', 'olena', '2014-02-28 14:56:48'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра теплоенергетичних установок теплових та атомних електростанцій''), ''25'')', 'olena', '2014-02-28 14:56:48'),
('INSERT INTO cafedra (name) VALUES (''Кафедра приладів та систем керування літальними апаратами '')', 'olena', '2014-02-28 14:57:22'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра приладів та систем керування літальними апаратами ''), ''4'')', 'olena', '2014-02-28 14:57:22'),
('INSERT INTO cafedra (name) VALUES (''Кафедра автоматизації експериментальних дослiджень'')', 'olena', '2014-02-28 14:58:04'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра автоматизації експериментальних дослiджень''), ''4'')', 'olena', '2014-02-28 14:58:04'),
('INSERT INTO cafedra (name) VALUES (''Кафедра інформацiйно-вимiрювальної техніки'')', 'olena', '2014-02-28 14:58:27'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра інформацiйно-вимiрювальної техніки''), ''4'')', 'olena', '2014-02-28 14:58:27'),
('INSERT INTO cafedra (name) VALUES (''Кафедра теоретичної механіки'')', 'olena', '2014-02-28 14:58:45'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра теоретичної механіки''), ''4'')', 'olena', '2014-02-28 14:58:45'),
('INSERT INTO cafedra (name) VALUES (''Кафедра біомедичної кібернетики'')', 'olena', '2014-02-28 14:59:58'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра біомедичної кібернетики''), ''18'')', 'olena', '2014-02-28 14:59:58'),
('INSERT INTO cafedra (name) VALUES (''Кафедра біомедичної інженерії'')', 'olena', '2014-02-28 15:00:18'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра біомедичної інженерії''), ''18'')', 'olena', '2014-02-28 15:00:18'),
('INSERT INTO cafedra (name) VALUES (''Кафедра біобезпеки і відновної біоінженерії'')', 'olena', '2014-02-28 15:00:31'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра біобезпеки і відновної біоінженерії''), ''18'')', 'olena', '2014-02-28 15:00:32'),
('INSERT INTO cafedra (name) VALUES (''Кафедра фізичної реабілітації'')', 'olena', '2014-02-28 15:00:53'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра фізичної реабілітації''), ''18'')', 'olena', '2014-02-28 15:00:53'),
('INSERT INTO cafedra (name) VALUES (''Кафедра фізичного виховання'')', 'olena', '2014-02-28 15:01:15'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра фізичного виховання''), ''18'')', 'olena', '2014-02-28 15:01:15'),
('INSERT INTO cafedra (name) VALUES (''Кафедра спортивного вдосконалення'')', 'olena', '2014-02-28 15:01:29'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра спортивного вдосконалення''), ''18'')', 'olena', '2014-02-28 15:01:29'),
('INSERT INTO cafedra (name) VALUES (''Кафедра промислової біотехнології'')', 'olena', '2014-02-28 15:01:54'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра промислової біотехнології''), ''19'')', 'olena', '2014-02-28 15:01:55'),
('INSERT INTO cafedra (name) VALUES (''Кафедра екобіотехнології та біоенергетики'')', 'olena', '2014-02-28 15:02:11'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра екобіотехнології та біоенергетики''), ''19'')', 'olena', '2014-02-28 15:02:12'),
('INSERT INTO cafedra (name) VALUES (''Кафедра біотехніки та інженерії'')', 'olena', '2014-02-28 15:02:27'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра біотехніки та інженерії''), ''19'')', 'olena', '2014-02-28 15:02:28'),
('INSERT INTO cafedra (name) VALUES (''Кафедра біоінформатики'')', 'olena', '2014-02-28 15:02:45'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра біоінформатики''), ''19'')', 'olena', '2014-02-28 15:02:45'),
('INSERT INTO cafedra (name) VALUES (''Кафедра електромеханіки'')', 'olena', '2014-02-28 15:03:27'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра електромеханіки''), ''20'')', 'olena', '2014-02-28 15:03:27'),
('INSERT INTO cafedra (name) VALUES (''Кафедра електричних станцій'')', 'olena', '2014-02-28 15:03:43'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра електричних станцій''), ''20'')', 'olena', '2014-02-28 15:03:43'),
('INSERT INTO cafedra (name) VALUES (''Кафедра автоматизації енергосистем'')', 'olena', '2014-02-28 15:04:04'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра автоматизації енергосистем''), ''20'')', 'olena', '2014-02-28 15:04:05'),
('INSERT INTO cafedra (name) VALUES (''Кафедра автоматизації електромеханічних систем та електроприводу '')', 'olena', '2014-02-28 15:04:30'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра автоматизації електромеханічних систем та електроприводу ''), ''20'')', 'olena', '2014-02-28 15:04:30'),
('INSERT INTO cafedra (name) VALUES (''Кафедра електричних мереж і систем'')', 'olena', '2014-02-28 15:04:52'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра електричних мереж і систем''), ''20'')', 'olena', '2014-02-28 15:04:52'),
('INSERT INTO cafedra (name) VALUES (''Кафедра техніки і електрофізики високих напруг'')', 'olena', '2014-02-28 15:05:08'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра техніки і електрофізики високих напруг''), ''20'')', 'olena', '2014-02-28 15:05:08'),
('INSERT INTO cafedra (name) VALUES (''Кафедра відновлювальних джерел енергії'')', 'olena', '2014-02-28 15:05:53'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра відновлювальних джерел енергії''), ''20'')', 'olena', '2014-02-28 15:05:53'),
('INSERT INTO cafedra (name) VALUES (''Кафедра теоретичної електротехніки'')', 'olena', '2014-02-28 15:06:12'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра теоретичної електротехніки''), ''20'')', 'olena', '2014-02-28 15:06:12'),
('INSERT INTO cafedra (name) VALUES (''Кафедра мікроелектроніки'')', 'olena', '2014-02-28 15:07:20'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра мікроелектроніки''), ''13'')', 'olena', '2014-02-28 15:07:20'),
('INSERT INTO cafedra (name) VALUES (''Кафедра електронних приладів та пристроїв'')', 'olena', '2014-02-28 15:07:37'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра електронних приладів та пристроїв''), ''13'')', 'olena', '2014-02-28 15:07:38'),
('INSERT INTO cafedra (name) VALUES (''Кафедра промислової електроніки'')', 'olena', '2014-02-28 15:07:56'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра промислової електроніки''), ''13'')', 'olena', '2014-02-28 15:07:57'),
('INSERT INTO cafedra (name) VALUES (''Кафедра фізичної та біомедичної електроніки'')', 'olena', '2014-02-28 15:08:14'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра фізичної та біомедичної електроніки''), ''13'')', 'olena', '2014-02-28 15:08:14'),
('INSERT INTO cafedra (name) VALUES (''Кафедра конструювання електронно-обчислювальної апаратури'')', 'olena', '2014-02-28 15:08:41'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра конструювання електронно-обчислювальної апаратури''), ''13'')', 'olena', '2014-02-28 15:08:41'),
('INSERT INTO cafedra (name) VALUES (''Кафедра акустики і акустоелектроніки'')', 'olena', '2014-02-28 15:09:01'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра акустики і акустоелектроніки''), ''13'')', 'olena', '2014-02-28 15:09:01'),
('INSERT INTO cafedra (name) VALUES (''Кафедра звукотехніки і реєстрації інформацiї'')', 'olena', '2014-02-28 15:09:20'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра звукотехніки і реєстрації інформацiї''), ''13'')', 'olena', '2014-02-28 15:09:20'),
('INSERT INTO cafedra (name) VALUES (''Кафедра автоматики i управлiння в технiчних системах'')', 'olena', '2014-02-28 15:09:56'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра автоматики i управлiння в технiчних системах''), ''1'')', 'olena', '2014-02-28 15:09:56'),
('INSERT INTO cafedra (name) VALUES (''Кафедра обчислювальної технiки'')', 'olena', '2014-02-28 15:10:15'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра обчислювальної технiки''), ''1'')', 'olena', '2014-02-28 15:10:15'),
('INSERT INTO cafedra (name) VALUES (''Кафедра технiчної кiбернетики'')', 'olena', '2014-02-28 15:10:28'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра технiчної кiбернетики''), ''1'')', 'olena', '2014-02-28 15:10:28'),
('INSERT INTO cafedra (name) VALUES (''Кафедра автоматизованих систем обробки iнформацiї та управлiння'')', 'olena', '2014-02-28 15:10:47'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра автоматизованих систем обробки iнформацiї та управлiння''), ''1'')', 'olena', '2014-02-28 15:10:47'),
('INSERT INTO cafedra (name) VALUES (''Кафедра теорії, практики та перекладу англійської мови'')', 'olena', '2014-02-28 15:12:45'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра теорії, практики та перекладу англійської мови''), ''5'')', 'olena', '2014-02-28 15:12:45'),
('INSERT INTO cafedra (name) VALUES (''Кафедра теорії, практики та перекладу французької мови'')', 'olena', '2014-02-28 15:13:10'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра теорії, практики та перекладу французької мови''), ''5'')', 'olena', '2014-02-28 15:13:10'),
('INSERT INTO cafedra (name) VALUES (''Кафедра теорії, практики та перекладу німецької мови '')', 'olena', '2014-02-28 15:13:35'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра теорії, практики та перекладу німецької мови ''), ''5'')', 'olena', '2014-02-28 15:13:35'),
('INSERT INTO cafedra (name) VALUES (''Кафедра менеджменту'')', 'olena', '2014-02-28 15:14:02'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра менеджменту''), ''8'')', 'olena', '2014-02-28 15:14:02'),
('INSERT INTO cafedra (name) VALUES (''Кафедра промислового маркетингу'')', 'olena', '2014-02-28 15:14:23'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра промислового маркетингу''), ''8'')', 'olena', '2014-02-28 15:14:23'),
('INSERT INTO cafedra (name) VALUES (''Кафедра математичного моделювання економічних систем '')', 'olena', '2014-02-28 15:14:49'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра математичного моделювання економічних систем ''), ''8'')', 'olena', '2014-02-28 15:14:50'),
('INSERT INTO cafedra (name) VALUES (''Кафедра економіки і підприємництва'')', 'olena', '2014-02-28 15:15:08'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра економіки і підприємництва''), ''8'')', 'olena', '2014-02-28 15:15:08'),
('INSERT INTO cafedra (name) VALUES (''Кафедра міжнародної економіки'')', 'olena', '2014-02-28 15:15:33'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра міжнародної економіки''), ''8'')', 'olena', '2014-02-28 15:15:33'),
('INSERT INTO cafedra (name) VALUES (''Кафедра математичного аналiзу та теорiї ймовiрностей '')', 'olena', '2014-02-28 15:16:31'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра математичного аналiзу та теорiї ймовiрностей ''), ''14'')', 'olena', '2014-02-28 15:16:31'),
('INSERT INTO cafedra (name) VALUES (''Кафедра диференціальних рiвнянь'')', 'olena', '2014-02-28 15:16:48'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра диференціальних рiвнянь''), ''14'')', 'olena', '2014-02-28 15:16:48'),
('INSERT INTO cafedra (name) VALUES (''Кафедра математичної фiзики'')', 'olena', '2014-02-28 15:17:08'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра математичної фiзики''), ''14'')', 'olena', '2014-02-28 15:17:08'),
('INSERT INTO cafedra (name) VALUES (''Кафедра загальної фiзики та фiзики твердого тiла'')', 'olena', '2014-02-28 15:17:25'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра загальної фiзики та фiзики твердого тiла''), ''14'')', 'olena', '2014-02-28 15:17:25'),
('INSERT INTO cafedra (name) VALUES (''Кафедра загальної та експериментальної фiзики'')', 'olena', '2014-02-28 15:17:51'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра загальної та експериментальної фiзики''), ''14'')', 'olena', '2014-02-28 15:17:51'),
('INSERT INTO cafedra (name) VALUES (''Кафедра загальної та теоретичної фiзики'')', 'olena', '2014-02-28 15:18:17'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра загальної та теоретичної фiзики''), ''14'')', 'olena', '2014-02-28 15:18:17'),
('INSERT INTO cafedra (name) VALUES (''Кафедра нарисної геометрiї, iнженерної та комп\\\\\\''ютерної графiки'')', 'olena', '2014-02-28 15:18:44'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра нарисної геометрiї, iнженерної та комп\\\\\\''ютерної графiки''), ''14'')', 'olena', '2014-02-28 15:18:44'),
('INSERT INTO cafedra (name) VALUES (''Кафедра прикладної математики'')', 'olena', '2014-02-28 15:19:21'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра прикладної математики''), ''9'')', 'olena', '2014-02-28 15:19:21'),
('INSERT INTO cafedra (name) VALUES (''Кафедра системного програмування і спеціалізованих комп’ютерних систем '')', 'olena', '2014-02-28 15:19:57'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра системного програмування і спеціалізованих комп’ютерних систем ''), ''9'')', 'olena', '2014-02-28 15:19:57'),
('INSERT INTO cafedra (name) VALUES (''Кафедра програмного забезпечення комп\\\\\\''ютерних систем'')', 'olena', '2014-02-28 15:20:30'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра програмного забезпечення комп\\\\\\''ютерних систем''), ''9'')', 'olena', '2014-02-28 15:20:30'),
('INSERT INTO cafedra (name) VALUES (''Кафедра соціології'')', 'olena', '2014-02-28 15:21:18'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра соціології''), ''16'')', 'olena', '2014-02-28 15:21:18'),
('INSERT INTO cafedra (name) VALUES (''Кафедра теорії та практики управління'')', 'olena', '2014-02-28 15:23:11'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра теорії та практики управління''), ''16'')', 'olena', '2014-02-28 15:23:11'),
('INSERT INTO cafedra (name) VALUES (''Кафедра публічного права'')', 'olena', '2014-02-28 15:23:36'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра публічного права''), ''16'')', 'olena', '2014-02-28 15:23:36'),
('INSERT INTO cafedra (name) VALUES (''Кафедра господарського та адміністративного права'')', 'olena', '2014-02-28 15:23:50'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра господарського та адміністративного права''), ''16'')', 'olena', '2014-02-28 15:23:50'),
('INSERT INTO cafedra (name) VALUES (''Кафедра інформаційного права та права інтелектуальної власності'')', 'olena', '2014-02-28 15:24:08'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра інформаційного права та права інтелектуальної власності''), ''16'')', 'olena', '2014-02-28 15:24:09'),
('INSERT INTO cafedra (name) VALUES (''Кафедра прикладної фізики'')', 'olena', '2014-02-28 15:24:39'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра прикладної фізики''), ''24'')', 'olena', '2014-02-28 15:24:40'),
('INSERT INTO cafedra (name) VALUES (''Кафедра інформаційної безпеки'')', 'olena', '2014-02-28 15:24:51'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра інформаційної безпеки''), ''24'')', 'olena', '2014-02-28 15:24:51'),
('INSERT INTO cafedra (name) VALUES (''Кафедра математичних методів захисту інформації'')', 'olena', '2014-02-28 15:25:07'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра математичних методів захисту інформації''), ''24'')', 'olena', '2014-02-28 15:25:07'),
('INSERT INTO cafedra (name) VALUES (''Кафедра фізико-технічних засобів захисту інформації '')', 'olena', '2014-02-28 15:25:39'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра фізико-технічних засобів захисту інформації ''), ''24'')', 'olena', '2014-02-28 15:25:39'),
('INSERT INTO cafedra (name) VALUES (''Кафедра технології неорганічних речовин та загальної хімічної технології'')', 'olena', '2014-02-28 15:26:15'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра технології неорганічних речовин та загальної хімічної технології''), ''6'')', 'olena', '2014-02-28 15:26:16'),
('INSERT INTO cafedra (name) VALUES (''Кафедра органічної хімії та технології органічних речовин'')', 'olena', '2014-02-28 15:26:36'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра органічної хімії та технології органічних речовин''), ''6'')', 'olena', '2014-02-28 15:26:36'),
('INSERT INTO cafedra (name) VALUES (''Кафедра технології електрохімічних виробництв'')', 'olena', '2014-02-28 15:26:59'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра технології електрохімічних виробництв''), ''6'')', 'olena', '2014-02-28 15:26:59'),
('INSERT INTO cafedra (name) VALUES (''Кафедра загальної та неорганічної хімії'')', 'olena', '2014-02-28 15:27:45'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра загальної та неорганічної хімії''), ''6'')', 'olena', '2014-02-28 15:27:45'),
('INSERT INTO cafedra (name) VALUES (''Кафедра фізичної хімії'')', 'olena', '2014-02-28 15:28:17'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра фізичної хімії''), ''6'')', 'olena', '2014-02-28 15:28:18'),
('INSERT INTO cafedra (name) VALUES (''Кафедра хімічної технології композиційних матеріалів'')', 'olena', '2014-02-28 15:28:40'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра хімічної технології композиційних матеріалів''), ''6'')', 'olena', '2014-02-28 15:28:40'),
('INSERT INTO cafedra (name) VALUES (''Кафедра хімічної технології кераміки та скла'')', 'olena', '2014-02-28 15:29:05'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра хімічної технології кераміки та скла''), ''6'')', 'olena', '2014-02-28 15:29:05'),
('INSERT INTO cafedra (name) VALUES (''Кафедра кібернетики хіміко-технологічних процесів'')', 'olena', '2014-02-28 15:29:22'),
('INSERT INTO transcafedra (cafedra, faculty) VALUES ((SELECT id FROM cafedra WHERE cafedra.name = ''Кафедра кібернетики хіміко-технологічних процесів''), ''6'')', 'olena', '2014-02-28 15:29:22');
INSERT INTO `log` (`l_info`, `u_name`, `l_tstamp`) VALUES
('user logon', 'olena', '2014-03-05 13:30:46'),
('INSERT INTO program (name) VALUES (''в рамках договору про співробітництво'')', 'olena', '2014-03-05 13:45:21'),
('INSERT INTO program (name) VALUES (''Програма подвійного диплому'')', 'olena', '2014-03-05 13:45:33'),
('INSERT INTO program (name) VALUES (''Програма подвійного диплому МОН'')', 'olena', '2014-03-05 13:45:44'),
('INSERT INTO program (name) VALUES (''EMERGE'')', 'olena', '2014-03-05 13:45:59'),
('INSERT INTO program (name) VALUES (''EUROEAST'')', 'olena', '2014-03-05 13:46:11'),
('INSERT INTO program (name) VALUES (''TEMPO'')', 'olena', '2014-03-05 13:46:24'),
('INSERT INTO program (name) VALUES (''EWENT'')', 'olena', '2014-03-05 13:46:39'),
('INSERT INTO program (name) VALUES (''ACTIVE'')', 'olena', '2014-03-05 13:47:12'),
('INSERT INTO program (name) VALUES (''[15:41:27] Violett: Індивідуальна магістерська програма'')', 'olena', '2014-03-05 13:47:40'),
('UPDATE program SET name=''Індивідуальна магістерська програма'' WHERE id = ''9''', 'olena', '2014-03-05 13:47:59'),
('INSERT INTO program (name) VALUES (''EUROASIA'')', 'olena', '2014-03-05 13:48:14'),
('INSERT INTO program (name) VALUES (''Quota Scheme'')', 'olena', '2014-03-05 13:49:23'),
('INSERT INTO program (name) VALUES (''Cтажування IAESTE'')', 'olena', '2014-03-05 13:49:38'),
('INSERT INTO program (name) VALUES (''стипендія \\"Георгіус Агрікола\\"'')', 'olena', '2014-03-05 13:49:59'),
('INSERT INTO financingsource (source) VALUES (''за власний рахунок'')', 'olena', '2014-03-05 13:51:44'),
('INSERT INTO financingsource (source) VALUES (''за рахунок приймаючої сторони'')', 'olena', '2014-03-05 13:52:23'),
('INSERT INTO financingsource (source) VALUES (''за рахунок державних коштів'')', 'olena', '2014-03-05 13:52:55'),
('INSERT INTO university (name, city, country) VALUES (''Університетський коледж м. Йовик'', ''м. Йовик'', ''Норвегія'')', 'olena', '2014-03-05 13:55:05'),
('UPDATE university SET name=''Університетський коледж Телемарк'', city = ''м. Порсгрунн'',\n		country = ''Норвегія'' WHERE id = ''1''', 'olena', '2014-03-05 13:55:16'),
('INSERT INTO university (name, city, country) VALUES (''Вища школа м. Мерзебург'', ''м. Мерзебург'', ''Німеччина'')', 'olena', '2014-03-05 13:55:53'),
('INSERT INTO university (name, city, country) VALUES (''Університет Гронінгену'', ''м. Гронінген'', ''Нідерланди'')', 'olena', '2014-03-05 13:56:41'),
('INSERT INTO university (name, city, country) VALUES (''Корейський інситут науки та технологій '', ''м. Сеул'', ''Корея'')', 'olena', '2014-03-05 13:57:11'),
('INSERT INTO university (name, city, country) VALUES (''Університет м. Ле Ман'', ''м. Ле Ман '', ''Франція'')', 'olena', '2014-03-05 13:57:36'),
('INSERT INTO university (name, city, country) VALUES (''Магдебурзький університет ім. Отто фон Геріке'', ''м. Магдебург'', ''Німеччина'')', 'olena', '2014-03-05 13:58:22'),
('INSERT INTO university (name, city, country) VALUES (''Політехнічна школа'', ''м. Париж'', ''Франція'')', 'olena', '2014-03-05 14:00:27'),
('INSERT INTO university (name, city, country) VALUES (''Університет Монтпельє 2'', ''м. Монтпельє'', ''Франція'')', 'olena', '2014-03-05 14:01:09'),
('INSERT INTO university (name, city, country) VALUES (''Будапештський університет технологій та економіки '', ''м. Будапешт'', ''Угорщина'')', 'olena', '2014-03-05 14:01:53'),
('INSERT INTO university (name, city, country) VALUES (''Технологічний унівесритет м. Дрезден'', ''м. Дрезден'', ''Німеччина'')', 'olena', '2014-03-05 14:02:35'),
('INSERT INTO university (name, city, country) VALUES (''Університет м. Гент'', ''м. Гент'', ''Бельгія'')', 'olena', '2014-03-05 14:02:54'),
('INSERT INTO university (name, city, country) VALUES (''Університет Аальто'', ''м. Аальто'', ''Фінляндія'')', 'olena', '2014-03-05 14:03:22'),
('INSERT INTO university (name, city, country) VALUES (''Технічний університет м. Лісабон'', ''м. Лісабон'', ''Португалія'')', 'olena', '2014-03-05 14:04:45'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''СМ-31с'', ''3'')', 'olena', '2014-03-05 14:14:18'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''СТ-31с'', ''3'')', 'olena', '2014-03-05 14:14:36'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''РП-31с'', ''3'')', 'olena', '2014-03-05 14:14:50'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''МВ-31с'', ''3'')', 'olena', '2014-03-05 14:15:24'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''ТМ-31с'', ''3'')', 'olena', '2014-03-05 14:15:37'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''СР-31с'', ''3'')', 'olena', '2014-03-05 14:15:54'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''СГ-31с'', ''3'')', 'olena', '2014-03-05 14:16:07'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''СМ-31м'', ''3'')', 'olena', '2014-03-05 14:16:37'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''СТ-31м'', ''3'')', 'olena', '2014-03-05 14:16:50'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''РП-31м'', ''3'')', 'olena', '2014-03-05 14:17:01'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''МВ-31м'', ''3'')', 'olena', '2014-03-05 14:17:13'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''ТМ-31м'', ''3'')', 'olena', '2014-03-05 14:17:27'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''СЕ-31м'', ''3'')', 'olena', '2014-03-05 14:17:42'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''СК-31м'', ''3'')', 'olena', '2014-03-05 14:17:53'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''СР-31м'', ''3'')', 'olena', '2014-03-05 14:18:04'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''РЗ-31м'', ''3'')', 'olena', '2014-03-05 14:18:15'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''СГ-31м'', ''3'')', 'olena', '2014-03-05 14:18:26'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''Ср-81м'', ''3'')', 'olena', '2014-03-05 14:18:45'),
('UPDATE mobilnist_db.grupa UT SET UT.name=''СР-81м'', UT.faculty = ''3'' WHERE UT.id = ''18''', 'olena', '2014-03-05 14:18:55'),
('UPDATE mobilnist_db.grupa UT SET UT.name=''СР-81м'', UT.faculty = ''3'' WHERE UT.id = ''18''', 'olena', '2014-03-05 14:21:46'),
('INSERT INTO university (name, city, country) VALUES (''Міланський політехнічний університет '', ''м. Мілан'', ''Італія'')', 'olena', '2014-03-05 14:50:56'),
('INSERT INTO university (name, city, country) VALUES (''Університет Лілля'', ''м. Ліль'', ''Франція'')', 'olena', '2014-03-05 14:53:36'),
('INSERT INTO university (name, city, country) VALUES (''Варшавська політехніка'', ''м. Варшава'', ''Польща'')', 'olena', '2014-03-05 14:54:36'),
('INSERT INTO university (name, city, country) VALUES (''Лаппеентрантський технологічний університет '', ''м. Лаппеентранта'', ''Фінляндія'')', 'olena', '2014-03-05 14:57:23'),
('INSERT INTO university (name, city, country) VALUES (''Єнський університет імені Фрідріха Шиллера'', ''м. Єна'', ''Німеччина'')', 'olena', '2014-03-05 14:58:56'),
('INSERT INTO university (name, city, country) VALUES (''Технічний університет Ліберець'', ''м. Ліберець'', ''Чехія'')', 'olena', '2014-03-05 15:01:34'),
('INSERT INTO university (name, city, country) VALUES (''Університет Павія'', ''м. Павія'', ''Італія'')', 'olena', '2014-03-05 15:04:49'),
('INSERT INTO university (name, city, country) VALUES (''Ягелонський університет м. Кракова'', ''м. Краків'', ''Польща'')', 'olena', '2014-03-05 15:07:44'),
('DELETE FROM student WHERE id = ''2''', 'olena', '2014-03-05 15:20:33'),
('INSERT INTO nakaz (number, date) VALUES (''88-с'', ''21.01.2014'')', 'olena', '2014-03-05 15:22:48'),
('DELETE FROM student WHERE id = ''1''', 'olena', '2014-03-05 15:24:25'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''ДП-31м'', ''13'')', 'olena', '2014-03-05 15:24:54'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Владислав'',''Володимирович'',''Медведєв'','''',''medvedev.vladislav@gmail.com'')', 'olena', '2014-03-05 15:25:49'),
('INSERT INTO main (typenap, nakaz, student, faculty, cafedra, main.grupa, course, napriam, university, departure , arrive, program, financingsource, diplom, financingcomment) VALUES (''3'', ''1'', ''3'', ''13'', ''77'',  ''19'', ''5'', ''3'', ''13'', ''03.02.2014'', ''30.06.2014'', ''5'', ''2'', '''', '''')', 'olena', '2014-03-05 15:26:42'),
('user logon', 'olena', '2014-03-06 07:39:41'),
('UPDATE student SET name=''Владислав'', middlename = ''Володимирович'', lastname = ''Медведєв'', contacts = ''medvedev.vladislav@gmail.com'' WHERE id = ''3''', 'olena', '2014-03-06 07:43:11'),
('INSERT INTO main (typenap, nakaz, student, faculty, cafedra, main.grupa, course, napriam, university, departure , arrive, program, financingsource, diplom, financingcomment) VALUES (''3'', ''1'', ''3'', ''13'', ''77'',  ''19'', ''5'', ''3'', ''13'', ''2014-02-03'', ''2014-06-30'', ''5'', ''2'', '''', '''')', 'olena', '2014-03-06 07:45:21'),
('UPDATE student SET name='''', middlename = '''', lastname = '''', contacts = '''' WHERE id = ''3''', 'olena', '2014-03-06 07:46:00'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Владислав'',''Володимирович'',''Медведєв'','''',''medvedev.vladislav@gmail.com'')', 'olena', '2014-03-06 07:46:30'),
('DELETE FROM student WHERE id = ''4''', 'olena', '2014-03-06 07:47:08'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Владислав'',''Володимирович'',''Медведєв'','''',''medvedev.vladislav@gmail.com'')', 'olena', '2014-03-06 07:48:46'),
('INSERT INTO main (typenap, nakaz, student, faculty, cafedra, main.grupa, course, napriam, university, departure , arrive, program, financingsource, diplom, financingcomment) VALUES (''3'', ''1'', ''5'', ''13'', ''77'',  ''19'', ''5'', ''3'', ''13'', ''2014-02-03'', ''2014-06-30'', ''5'', ''2'', '''', '''')', 'olena', '2014-03-06 07:49:19'),
('user logon', 'olena', '2014-03-06 07:53:40'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Анна'',''Володимирівна'',''Малашихіна'','''',''no_parsley@i.ua 0630597932'')', 'olena', '2014-03-06 07:55:19'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''КВ-84м'', ''9'')', 'olena', '2014-03-06 07:57:29'),
('UPDATE cafedra SET name=''Кафедра програмного забезпечення комп'' WHERE id = ''105''', 'olena', '2014-03-06 07:57:56'),
('UPDATE cafedra SET name=''Кафедра програмного забезпечення комп`ютерних систем'' WHERE id = ''105''', 'olena', '2014-03-06 07:58:36'),
('INSERT INTO nakaz (number, date) VALUES (''208-с'', ''30.01.2014'')', 'olena', '2014-03-06 08:01:03'),
('INSERT INTO main (typenap, nakaz, student, faculty, cafedra, main.grupa, course, napriam, university, departure , arrive, program, financingsource, diplom, financingcomment) VALUES (''3'', ''2'', ''6'', ''9'', ''105'',  ''20'', ''6'', ''3'', ''2'', ''2014-02-03'', ''2014-03-02'', ''1'', ''2'', '''', '''')', 'olena', '2014-03-06 08:02:20'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Артем'',''Леонідович'',''Арсенічев'','''',''kick1927@mail.ru 0971027896'')', 'olena', '2014-03-06 08:06:39'),
('INSERT INTO nakaz (number, date) VALUES (''95-с'', ''22.01.2014'')', 'olena', '2014-03-06 08:08:35'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''ЛА-31м'', ''23'')', 'olena', '2014-03-06 08:10:04'),
('INSERT INTO main (typenap, nakaz, student, faculty, cafedra, main.grupa, course, napriam, university, departure , arrive, program, financingsource, diplom, financingcomment) VALUES (''3'', ''3'', ''7'', ''23'', ''29'',  ''21'', ''5'', ''3'', ''15'', ''2014-02-03'', ''2014-06-29'', ''6'', ''2'', '''', '''')', 'olena', '2014-03-06 08:10:58'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Даніїл '',''Володимирович'',''Брох'','''',''0958727292'')', 'olena', '2014-03-06 08:13:33'),
('INSERT INTO main (typenap, nakaz, student, faculty, cafedra, main.grupa, course, napriam, university, departure , arrive, program, financingsource, diplom, financingcomment) VALUES (''3'', ''3'', ''8'', ''23'', ''29'',  ''21'', ''5'', ''3'', ''15'', ''2014-02-03'', ''2014-06-'', ''6'', ''2'', '''', '''')', 'olena', '2014-03-06 08:15:20'),
('UPDATE nakaz SET number=''208-с'', date = ''2014-01-30'' WHERE id = ''2''', 'olena', '2014-03-06 08:16:26'),
('UPDATE nakaz SET number=''88-с'', date = ''2014-01-21'' WHERE id = ''1''', 'olena', '2014-03-06 08:16:46'),
('UPDATE nakaz SET number=''95-с'', date = ''2014-01-22'' WHERE id = ''3''', 'olena', '2014-03-06 08:16:59'),
('INSERT INTO main (typenap, nakaz, student, faculty, cafedra, main.grupa, course, napriam, university, departure , arrive, program, financingsource, diplom, financingcomment) VALUES (''3'', ''3'', ''8'', ''23'', ''29'',  ''21'', ''5'', ''3'', ''15'', ''2014-02-03'', ''2014-06-29'', ''6'', ''2'', '''', '''')', 'olena', '2014-03-06 08:18:01'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Вадим'',''Петрович'',''Дяченко'','''',''barv@barv.org.ua 0636636505'')', 'olena', '2014-03-06 08:23:00'),
('INSERT INTO nakaz (number, date) VALUES (''107-c'', ''2014-01-23'')', 'olena', '2014-03-06 08:24:45'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''ПІ-81м'', ''12'')', 'olena', '2014-03-06 08:25:21'),
('UPDATE student SET name=''Вадим'', middlename = ''Петрович'', lastname = ''Дяченко'', contacts = ''barv@barv.org.ua 0636636505'' WHERE id = ''9''', 'olena', '2014-03-06 08:25:31'),
('INSERT INTO main (typenap, nakaz, student, faculty, cafedra, main.grupa, course, napriam, university, departure , arrive, program, financingsource, diplom, financingcomment) VALUES (''3'', ''4'', ''9'', ''12'', ''40'',  ''22'', ''6'', ''3'', ''10'', ''2014-02-03'', ''2014-02-28'', ''7'', ''2'', '''', '''')', 'olena', '2014-03-06 08:26:19'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''ДЕ-31м'', ''13'')', 'olena', '2014-03-06 08:28:54'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Олександр'',''Олександрович'',''Бурима '','''',''alexander.buryma@gmail.com Skype: aleksions'')', 'olena', '2014-03-06 08:35:36'),
('INSERT INTO nakaz (number, date) VALUES (''164-c'', ''2014-01-30'')', 'olena', '2014-03-06 08:37:09'),
('INSERT INTO main (typenap, nakaz, student, faculty, cafedra, main.grupa, course, napriam, university, departure , arrive, program, financingsource, diplom, financingcomment) VALUES (''3'', ''5'', ''10'', ''13'', ''78'',  ''23'', ''5'', ''3'', ''14'', ''2014-02-10'', ''2014-06-30'', ''5'', ''2'', '''', '''')', 'olena', '2014-03-06 08:37:54'),
('INSERT INTO nakaz (number, date) VALUES (''81/2c'', ''2014-01-29'')', 'olena', '2014-03-06 08:38:45'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''ІО-81м'', ''1'')', 'olena', '2014-03-06 08:39:10'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Олександр'',''Юрійович'',''Якушев'','''','''')', 'olena', '2014-03-06 08:39:55'),
('INSERT INTO main (typenap, nakaz, student, faculty, cafedra, main.grupa, course, napriam, university, departure , arrive, program, financingsource, diplom, financingcomment) VALUES (''3'', ''6'', ''11'', ''1'', ''85'',  ''24'', ''6'', ''3'', ''2'', ''2014-02-03'', ''2014-03-02'', ''11'', ''2'', '''', '''')', 'olena', '2014-03-06 08:41:15'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Наталія'',''Володимирівна'',''Уварова'','''','''')', 'olena', '2014-03-06 08:42:16'),
('INSERT INTO main (typenap, nakaz, student, faculty, cafedra, main.grupa, course, napriam, university, departure , arrive, program, financingsource, diplom, financingcomment) VALUES (''3'', ''6'', ''12'', ''1'', ''85'',  ''24'', ''6'', ''3'', ''2'', ''2014-02-03'', ''2014-03-02'', ''11'', ''2'', '''', '''')', 'olena', '2014-03-06 08:43:21'),
('INSERT INTO nakaz (number, date) VALUES (''199с'', ''2014-01-30'')', 'olena', '2014-03-06 08:45:47'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''УВ-31м'', ''8'')', 'olena', '2014-03-06 08:46:12'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Ігор'',''Сергійович'',''Брижатий'','''','''')', 'olena', '2014-03-06 08:46:44'),
('INSERT INTO program (name) VALUES (''IANUS'')', 'olena', '2014-03-06 08:48:22'),
('INSERT INTO main (typenap, nakaz, student, faculty, cafedra, main.grupa, course, napriam, university, departure , arrive, program, financingsource, diplom, financingcomment) VALUES (''3'', ''7'', ''13'', ''8'', ''91'',  ''25'', ''5'', ''3'', ''16'', ''2014-02-10'', ''2014-06-30'', ''14'', ''2'', '''', '''')', 'olena', '2014-03-06 08:49:07'),
('INSERT INTO nakaz (number, date) VALUES (''184-c'', ''2014-01-30'')', 'olena', '2014-03-06 08:49:58'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''ТЗ-81м'', ''2'')', 'olena', '2014-03-06 08:50:24'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Олег'',''Юрійович'',''Лавріненко'','''',''lavrinenko_oleh@mail.ru 0964941848'')', 'olena', '2014-03-06 08:53:07'),
('INSERT INTO main (typenap, nakaz, student, faculty, cafedra, main.grupa, course, napriam, university, departure , arrive, program, financingsource, diplom, financingcomment) VALUES (''3'', ''8'', ''14'', ''2'', ''1'',  ''26'', ''6'', ''3'', ''11'', ''2014-02-03'', ''2014-03-02'', ''13'', ''2'', '''', '''')', 'olena', '2014-03-06 08:55:15'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''ТС-81м'', ''2'')', 'olena', '2014-03-06 08:58:16'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Людмила'',''Олександрівна'',''Буркан'','''',''lyudaburkan@gmail.com'')', 'olena', '2014-03-06 09:06:30'),
('INSERT INTO main (typenap, nakaz, student, faculty, cafedra, main.grupa, course, napriam, university, departure , arrive, program, financingsource, diplom, financingcomment) VALUES (''3'', ''8'', ''15'', ''2'', ''2'',  ''27'', ''6'', ''3'', ''11'', ''2014-02-03'', ''2014-03-02'', ''13'', ''2'', '''', '''')', 'olena', '2014-03-06 09:10:07'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''ТС-83м'', ''2'')', 'olena', '2014-03-06 09:18:26'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Олексій'',''Федорович'',''Орган'','''',''organ_a@ukr.net  0974806370'')', 'olena', '2014-03-06 09:19:40'),
('INSERT INTO main (typenap, nakaz, student, faculty, cafedra, main.grupa, course, napriam, university, departure , arrive, program, financingsource, diplom, financingcomment) VALUES (''3'', ''8'', ''16'', ''2'', ''1'',  ''28'', ''6'', ''3'', ''11'', ''2014-02-03'', ''2014-03-02'', ''13'', ''2'', '''', '''')', 'olena', '2014-03-06 09:22:42'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''КВ-83м'', ''9'')', 'olena', '2014-03-06 09:25:26'),
('INSERT INTO nakaz (number, date) VALUES (''209-с'', ''2014-01-30'')', 'olena', '2014-03-06 09:26:01'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Артем'',''Вячеславович'',''Якименко'','''','''')', 'olena', '2014-03-06 09:31:46'),
('INSERT INTO main (typenap, nakaz, student, faculty, cafedra, main.grupa, course, napriam, university, departure , arrive, program, financingsource, diplom, financingcomment) VALUES (''3'', ''9'', ''17'', ''9'', ''104'',  ''29'', ''6'', ''3'', ''6'', ''2014-02-03'', ''2014-03-02'', ''3'', ''3'', '''', '''')', 'olena', '2014-03-06 09:33:46'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''КВ-81м'', ''9'')', 'olena', '2014-03-06 09:46:02'),
('INSERT INTO nakaz (number, date) VALUES (''210-с'', ''2014-01-30'')', 'olena', '2014-03-06 09:46:33'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Гіясіддін'',''Алігавхарович'',''Бахтоваршоєв '','''','''')', 'olena', '2014-03-06 09:47:30'),
('INSERT INTO main (typenap, nakaz, student, faculty, cafedra, main.grupa, course, napriam, university, departure , arrive, program, financingsource, diplom, financingcomment) VALUES (''3'', ''10'', ''18'', ''9'', ''104'',  ''30'', ''6'', ''3'', ''8'', ''2014-02-03'', ''2014-03-02'', ''1'', ''2'', '''', '''')', 'olena', '2014-03-06 09:49:26'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''ХП-81м'', ''6'')', 'olena', '2014-03-06 13:33:18'),
('INSERT INTO nakaz (number, date) VALUES (''227-с'', ''2014-01-31'')', 'olena', '2014-03-06 13:33:42'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Роман'',''Вадимович'',''Мілоцький '','''','''')', 'olena', '2014-03-06 13:34:36'),
('INSERT INTO main (typenap, nakaz, student, faculty, cafedra, main.grupa, course, napriam, university, departure , arrive, program, financingsource, diplom, financingcomment) VALUES (''3'', ''11'', ''19'', ''6'', ''122'',  ''31'', ''6'', ''3'', ''6'', ''2014-02-03'', ''2014-03-02'', ''2'', ''1'', '''', '''')', 'olena', '2014-03-06 13:35:52'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''ХО-81м'', ''6'')', 'olena', '2014-03-06 14:04:25'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Катерина'',''Олександрівна'',''Омельчук'','''',''omelchuk.ketrin.ua@gmail.com 0984090110'')', 'olena', '2014-03-06 14:05:37'),
('INSERT INTO main (typenap, nakaz, student, faculty, cafedra, main.grupa, course, napriam, university, departure , arrive, program, financingsource, diplom, financingcomment) VALUES (''3'', ''11'', ''20'', ''6'', ''117'',  ''32'', ''6'', ''3'', ''6'', ''2014-02-03'', ''2014-03-02'', ''2'', ''1'', '''', '''')', 'olena', '2014-03-06 14:06:48'),
('user logon', 'olena', '2014-03-07 07:25:54'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''ТК-81м'', ''25'')', 'olena', '2014-03-07 08:45:57'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Олексій'',''Олександрович'',''Сігал'','''',''alex.a.sigal@gmaul.com'')', 'olena', '2014-03-07 08:46:54'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''ХЕ-31м'', ''6'')', 'olena', '2014-03-07 08:49:06'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Світлана'',''Олегівна'',''Ярова'','''',''svitlana.yarova@hotmail.com'')', 'olena', '2014-03-07 08:50:14'),
('INSERT INTO mobilnist_db.grupa (name, faculty) VALUES (''ХО-31м'', ''6'')', 'olena', '2014-03-07 09:03:00'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Юрій'',''Едуардович'',''Максимюк '','''','''')', 'olena', '2014-03-07 09:03:40'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Богдан'',''Анатолійович'',''Білецький'','''',''bogdan.biletskyi@gmail.com'')', 'olena', '2014-03-07 09:11:02'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Роман'',''Олександрович'',''Селін'','''',''roman.selin@mail.ru'')', 'olena', '2014-03-07 09:14:37'),
('INSERT INTO student (name, middlename, lastname, date, contacts) VALUES (''Владислав'',''Валерійович'',''Тезик'','''',''vlad.tezyk@gmail.com 0671971245'')', 'olena', '2014-03-07 09:23:12');

-- --------------------------------------------------------

--
-- Структура таблицы `main`
--

CREATE TABLE IF NOT EXISTS `main` (
  `typenap` tinyint(4) DEFAULT NULL,
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nakaz` bigint(20) unsigned NOT NULL,
  `student` bigint(20) unsigned NOT NULL,
  `faculty` bigint(20) unsigned NOT NULL,
  `cafedra` bigint(20) unsigned NOT NULL,
  `grupa` bigint(20) unsigned NOT NULL,
  `course` tinyint(4) NOT NULL,
  `napriam` bigint(20) unsigned NOT NULL,
  `university` bigint(20) unsigned NOT NULL,
  `departure` date NOT NULL,
  `arrive` date NOT NULL,
  `program` bigint(20) unsigned NOT NULL,
  `financingsource` bigint(20) unsigned NOT NULL,
  `diplom` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `financingcomment` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `main` (`typenap`,`nakaz`,`student`,`faculty`,`cafedra`,`grupa`,`course`,`napriam`,`university`,`departure`,`arrive`,`program`,`financingsource`),
  KEY `nakaz` (`nakaz`),
  KEY `student` (`student`),
  KEY `faculty` (`faculty`),
  KEY `cafedra` (`cafedra`),
  KEY `grupa` (`grupa`),
  KEY `napriam` (`napriam`),
  KEY `university` (`university`),
  KEY `program` (`program`),
  KEY `financingsource` (`financingsource`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=36 ;

--
-- Дамп данных таблицы `main`
--

INSERT INTO `main` (`typenap`, `id`, `nakaz`, `student`, `faculty`, `cafedra`, `grupa`, `course`, `napriam`, `university`, `departure`, `arrive`, `program`, `financingsource`, `diplom`, `financingcomment`) VALUES
(3, 8, 1, 5, 13, 77, 19, 5, 3, 13, '2014-02-03', '2014-06-30', 5, 2, '', ''),
(3, 10, 2, 6, 9, 105, 20, 6, 3, 2, '2014-02-03', '2014-03-02', 1, 2, '', ''),
(3, 15, 3, 7, 23, 29, 21, 5, 3, 15, '2014-02-03', '2014-06-29', 6, 2, '', ''),
(3, 17, 3, 8, 23, 29, 21, 5, 3, 15, '2014-02-03', '2014-06-29', 6, 2, '', ''),
(3, 20, 4, 9, 12, 40, 22, 6, 3, 10, '2014-02-03', '2014-02-28', 7, 2, '', ''),
(3, 21, 5, 10, 13, 78, 23, 5, 3, 14, '2014-02-10', '2014-06-30', 5, 2, '', ''),
(3, 22, 6, 11, 1, 85, 24, 6, 3, 2, '2014-02-03', '2014-03-02', 11, 2, '', ''),
(3, 23, 6, 12, 1, 85, 24, 6, 3, 2, '2014-02-03', '2014-03-02', 11, 2, '', ''),
(3, 25, 7, 13, 8, 91, 25, 5, 3, 16, '2014-02-10', '2014-06-30', 14, 2, '', ''),
(3, 26, 8, 14, 2, 1, 26, 6, 3, 11, '2014-02-03', '2014-03-02', 13, 2, '', ''),
(3, 27, 8, 15, 2, 2, 27, 6, 3, 11, '2014-02-03', '2014-03-02', 13, 2, '', ''),
(3, 28, 8, 16, 2, 1, 28, 6, 3, 11, '2014-02-03', '2014-03-02', 13, 2, '', ''),
(3, 29, 9, 17, 9, 104, 29, 6, 3, 6, '2014-02-03', '2014-03-02', 3, 3, '', ''),
(3, 30, 10, 18, 9, 104, 30, 6, 3, 8, '2014-02-03', '2014-03-02', 1, 2, '', ''),
(3, 31, 11, 19, 6, 122, 31, 6, 3, 6, '2014-02-03', '2014-03-02', 2, 1, '', ''),
(3, 32, 11, 20, 6, 117, 32, 6, 3, 6, '2014-02-03', '2014-03-02', 2, 1, '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `nakaz`
--

CREATE TABLE IF NOT EXISTS `nakaz` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `number` (`number`,`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `nakaz`
--

INSERT INTO `nakaz` (`id`, `number`, `date`) VALUES
(4, '107-c', '2014-01-23'),
(5, '164-c', '2014-01-30'),
(8, '184-c', '2014-01-30'),
(7, '199с', '2014-01-30'),
(2, '208-с', '2014-01-30'),
(9, '209-с', '2014-01-30'),
(10, '210-с', '2014-01-30'),
(11, '227-с', '2014-01-31'),
(6, '81/2c', '2014-01-29'),
(1, '88-с', '2014-01-21'),
(3, '95-с', '2014-01-22');

-- --------------------------------------------------------

--
-- Структура таблицы `napriam`
--

CREATE TABLE IF NOT EXISTS `napriam` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `napriam`
--

INSERT INTO `napriam` (`id`, `name`) VALUES
(1, 'бакалавр'),
(3, 'магістр'),
(2, 'спеціаліст');

-- --------------------------------------------------------

--
-- Структура таблицы `program`
--

CREATE TABLE IF NOT EXISTS `program` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `program`
--

INSERT INTO `program` (`id`, `name`) VALUES
(8, 'ACTIVE'),
(12, 'Cтажування IAESTE'),
(4, 'EMERGE'),
(10, 'EUROASIA'),
(5, 'EUROEAST'),
(7, 'EWENT'),
(14, 'IANUS'),
(11, 'Quota Scheme'),
(6, 'TEMPO'),
(1, 'в рамках договору про співробітництво'),
(9, 'Індивідуальна магістерська програма'),
(2, 'Програма подвійного диплому'),
(3, 'Програма подвійного диплому МОН'),
(13, 'стипендія "Георгіус Агрікола"');

-- --------------------------------------------------------

--
-- Структура таблицы `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `middlename` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `contacts` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `student` (`name`,`middlename`,`lastname`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=27 ;

--
-- Дамп данных таблицы `student`
--

INSERT INTO `student` (`id`, `name`, `middlename`, `lastname`, `date`, `contacts`) VALUES
(3, '', '', '', '0000-00-00', ''),
(5, 'Владислав', 'Володимирович', 'Медведєв', '0000-00-00', 'medvedev.vladislav@gmail.com'),
(6, 'Анна', 'Володимирівна', 'Малашихіна', '0000-00-00', 'no_parsley@i.ua 0630597932'),
(7, 'Артем', 'Леонідович', 'Арсенічев', '0000-00-00', 'kick1927@mail.ru 0971027896'),
(8, 'Даніїл ', 'Володимирович', 'Брох', '0000-00-00', '0958727292'),
(9, 'Вадим', 'Петрович', 'Дяченко', '0000-00-00', 'barv@barv.org.ua 0636636505'),
(10, 'Олександр', 'Олександрович', 'Бурима ', '0000-00-00', 'alexander.buryma@gmail.com Skype: aleksions'),
(11, 'Олександр', 'Юрійович', 'Якушев', '0000-00-00', ''),
(12, 'Наталія', 'Володимирівна', 'Уварова', '0000-00-00', ''),
(13, 'Ігор', 'Сергійович', 'Брижатий', '0000-00-00', ''),
(14, 'Олег', 'Юрійович', 'Лавріненко', '0000-00-00', 'lavrinenko_oleh@mail.ru 0964941848'),
(15, 'Людмила', 'Олександрівна', 'Буркан', '0000-00-00', 'lyudaburkan@gmail.com'),
(16, 'Олексій', 'Федорович', 'Орган', '0000-00-00', 'organ_a@ukr.net  0974806370'),
(17, 'Артем', 'Вячеславович', 'Якименко', '0000-00-00', ''),
(18, 'Гіясіддін', 'Алігавхарович', 'Бахтоваршоєв ', '0000-00-00', ''),
(19, 'Роман', 'Вадимович', 'Мілоцький ', '0000-00-00', ''),
(20, 'Катерина', 'Олександрівна', 'Омельчук', '0000-00-00', 'omelchuk.ketrin.ua@gmail.com 0984090110'),
(21, 'Олексій', 'Олександрович', 'Сігал', '0000-00-00', 'alex.a.sigal@gmaul.com'),
(22, 'Світлана', 'Олегівна', 'Ярова', '0000-00-00', 'svitlana.yarova@hotmail.com'),
(23, 'Юрій', 'Едуардович', 'Максимюк ', '0000-00-00', ''),
(24, 'Богдан', 'Анатолійович', 'Білецький', '0000-00-00', 'bogdan.biletskyi@gmail.com'),
(25, 'Роман', 'Олександрович', 'Селін', '0000-00-00', 'roman.selin@mail.ru'),
(26, 'Владислав', 'Валерійович', 'Тезик', '0000-00-00', 'vlad.tezyk@gmail.com 0671971245');

-- --------------------------------------------------------

--
-- Структура таблицы `tmptable`
--

CREATE TABLE IF NOT EXISTS `tmptable` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `middlename` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `faculty` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course` tinyint(10) DEFAULT NULL,
  `groupa` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cafedra` varchar(2048) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `university` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `departure` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dep` date DEFAULT NULL,
  `arr` date DEFAULT NULL,
  `arrive` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `program` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nakaz` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `naknum` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nakdate` date DEFAULT NULL,
  `napriam` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `finsource` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contacts` varchar(2048) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comments` varchar(2048) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fincomment` varchar(2048) COLLATE utf8_unicode_ci DEFAULT NULL,
  `diplom` varchar(2048) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timeofstudy` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `filename` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `transcafedra`
--

CREATE TABLE IF NOT EXISTS `transcafedra` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cafedra` bigint(20) unsigned NOT NULL,
  `faculty` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `transcafedra` (`cafedra`,`faculty`),
  KEY `cafedra` (`cafedra`),
  KEY `faculty` (`faculty`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=122 ;

--
-- Дамп данных таблицы `transcafedra`
--

INSERT INTO `transcafedra` (`id`, `cafedra`, `faculty`) VALUES
(1, 1, 2),
(2, 2, 2),
(3, 3, 2),
(4, 4, 3),
(5, 5, 3),
(6, 6, 3),
(7, 8, 3),
(8, 9, 3),
(9, 10, 3),
(10, 11, 22),
(11, 12, 22),
(12, 13, 22),
(13, 14, 10),
(14, 15, 10),
(15, 16, 10),
(16, 17, 10),
(17, 18, 10),
(18, 19, 10),
(19, 20, 10),
(20, 21, 7),
(21, 22, 7),
(22, 23, 11),
(23, 24, 11),
(24, 25, 11),
(25, 26, 11),
(26, 27, 11),
(27, 28, 23),
(28, 29, 23),
(29, 30, 23),
(30, 31, 23),
(31, 32, 21),
(32, 33, 21),
(33, 34, 21),
(34, 35, 21),
(35, 36, 21),
(36, 37, 21),
(37, 38, 21),
(38, 39, 21),
(39, 40, 12),
(40, 41, 12),
(41, 42, 12),
(42, 43, 12),
(43, 44, 12),
(44, 45, 12),
(45, 46, 15),
(46, 47, 15),
(47, 48, 15),
(48, 49, 15),
(49, 50, 25),
(50, 51, 25),
(51, 52, 25),
(52, 53, 25),
(53, 54, 25),
(54, 55, 4),
(55, 56, 4),
(56, 57, 4),
(57, 58, 4),
(58, 59, 18),
(59, 60, 18),
(60, 61, 18),
(61, 62, 18),
(62, 63, 18),
(63, 64, 18),
(64, 65, 19),
(65, 66, 19),
(66, 67, 19),
(67, 68, 19),
(68, 69, 20),
(69, 70, 20),
(70, 71, 20),
(71, 72, 20),
(72, 73, 20),
(73, 74, 20),
(74, 75, 20),
(75, 76, 20),
(76, 77, 13),
(77, 78, 13),
(78, 79, 13),
(79, 80, 13),
(80, 81, 13),
(81, 82, 13),
(82, 83, 13),
(83, 84, 1),
(84, 85, 1),
(85, 86, 1),
(86, 87, 1),
(87, 88, 5),
(88, 89, 5),
(89, 90, 5),
(90, 91, 8),
(91, 92, 8),
(92, 93, 8),
(93, 94, 8),
(94, 95, 8),
(95, 96, 14),
(96, 97, 14),
(97, 98, 14),
(98, 99, 14),
(99, 100, 14),
(100, 101, 14),
(101, 102, 14),
(102, 103, 9),
(103, 104, 9),
(104, 105, 9),
(105, 106, 16),
(106, 108, 16),
(107, 109, 16),
(108, 110, 16),
(109, 111, 16),
(110, 112, 24),
(111, 113, 24),
(112, 114, 24),
(113, 115, 24),
(114, 116, 6),
(115, 117, 6),
(116, 118, 6),
(117, 119, 6),
(118, 121, 6),
(119, 122, 6),
(120, 123, 6),
(121, 124, 6);

-- --------------------------------------------------------

--
-- Структура таблицы `university`
--

CREATE TABLE IF NOT EXISTS `university` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`,`city`,`country`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

--
-- Дамп данных таблицы `university`
--

INSERT INTO `university` (`id`, `name`, `city`, `country`) VALUES
(10, 'Будапештський університет технологій та економіки ', 'м. Будапешт', 'Угорщина'),
(17, 'Варшавська політехніка', 'м. Варшава', 'Польща'),
(3, 'Вища школа м. Мерзебург', 'м. Мерзебург', 'Німеччина'),
(19, 'Єнський університет імені Фрідріха Шиллера', 'м. Єна', 'Німеччина'),
(5, 'Корейський інситут науки та технологій ', 'м. Сеул', 'Корея'),
(18, 'Лаппеентрантський технологічний університет ', 'м. Лаппеентранта', 'Фінляндія'),
(7, 'Магдебурзький університет ім. Отто фон Геріке', 'м. Магдебург', 'Німеччина'),
(15, 'Міланський політехнічний університет ', 'м. Мілан', 'Італія'),
(8, 'Політехнічна школа', 'м. Париж', 'Франція'),
(20, 'Технічний університет Ліберець', 'м. Ліберець', 'Чехія'),
(14, 'Технічний університет м. Лісабон', 'м. Лісабон', 'Португалія'),
(11, 'Технологічний унівесритет м. Дрезден', 'м. Дрезден', 'Німеччина'),
(13, 'Університет Аальто', 'м. Аальто', 'Фінляндія'),
(4, 'Університет Гронінгену', 'м. Гронінген', 'Нідерланди'),
(16, 'Університет Лілля', 'м. Ліль', 'Франція'),
(12, 'Університет м. Гент', 'м. Гент', 'Бельгія'),
(6, 'Університет м. Ле Ман', 'м. Ле Ман ', 'Франція'),
(9, 'Університет Монтпельє 2', 'м. Монтпельє', 'Франція'),
(21, 'Університет Павія', 'м. Павія', 'Італія'),
(2, 'Університетський коледж м. Йовик', 'м. Йовик', 'Норвегія'),
(1, 'Університетський коледж Телемарк', 'м. Порсгрунн', 'Норвегія'),
(22, 'Ягелонський університет м. Кракова', 'м. Краків', 'Польща');

-- --------------------------------------------------------

--
-- Структура таблицы `user_list`
--

CREATE TABLE IF NOT EXISTS `user_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `u_pass` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `u_type` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_name` (`u_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `user_list`
--

INSERT INTO `user_list` (`id`, `u_name`, `u_pass`, `u_type`) VALUES
(1, 'super', '732b14cc97a6154663ea57f83bc017ab', 1),
(2, 'olena', 'af53068915dfb84a788c6bc218db2347', 0);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `grupa`
--
ALTER TABLE `grupa`
  ADD CONSTRAINT `grupa_FK_faculty` FOREIGN KEY (`faculty`) REFERENCES `faculty` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `main`
--
ALTER TABLE `main`
  ADD CONSTRAINT `main_FK_Cafedra` FOREIGN KEY (`cafedra`) REFERENCES `cafedra` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `main_FK_Faculty` FOREIGN KEY (`faculty`) REFERENCES `faculty` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `main_FK_financingsource` FOREIGN KEY (`financingsource`) REFERENCES `financingsource` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `main_FK_Grupa` FOREIGN KEY (`grupa`) REFERENCES `grupa` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `main_FK_nakaz` FOREIGN KEY (`nakaz`) REFERENCES `nakaz` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `main_FK_napriam` FOREIGN KEY (`napriam`) REFERENCES `napriam` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `main_FK_program` FOREIGN KEY (`program`) REFERENCES `program` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `main_FK_student` FOREIGN KEY (`student`) REFERENCES `student` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `main_FK_university` FOREIGN KEY (`university`) REFERENCES `university` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `transcafedra`
--
ALTER TABLE `transcafedra`
  ADD CONSTRAINT `trcaf_FK_cafedra` FOREIGN KEY (`cafedra`) REFERENCES `cafedra` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `trcaf_FK_faculty` FOREIGN KEY (`faculty`) REFERENCES `faculty` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
