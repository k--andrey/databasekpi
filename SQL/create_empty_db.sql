-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 04, 2013 at 02:37 PM
-- Server version: 5.5.31
-- PHP Version: 5.4.4-14+deb7u5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mobilnist_db`
--

DROP DATABASE mobilnist_db;
CREATE DATABASE mobilnist_db COLLATE utf8_unicode_ci;
USE mobilnist_db;

-- --------------------------------------------------------

--
-- Table structure for table `cafedra`
--

CREATE TABLE IF NOT EXISTS `cafedra` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE IF NOT EXISTS `faculty` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;

-- --------------------------------------------------------

--
-- Table structure for table `financingsource`
--

CREATE TABLE IF NOT EXISTS `financingsource` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `source` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `source` (`source`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;

-- --------------------------------------------------------

--
-- Table structure for table `grupa`
--

CREATE TABLE IF NOT EXISTS `grupa` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `faculty` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `faculty` (`faculty`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8  COLLATE=utf8_unicode_ci ;

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `l_info` varchar(512) DEFAULT NULL,
  `u_name` varchar(50) DEFAULT NULL,
  `l_tstamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8  COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `main`
--

CREATE TABLE IF NOT EXISTS `main` (
  `typenap` tinyint(4) DEFAULT NULL,
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nakaz` bigint(20) unsigned NOT NULL,
  `student` bigint(20) unsigned NOT NULL,
  `faculty` bigint(20) unsigned NOT NULL,
  `cafedra` bigint(20) unsigned NOT NULL,
  `grupa` bigint(20) unsigned NOT NULL,
  `course` tinyint(4) NOT NULL,
  `napriam` bigint(20) unsigned NOT NULL,
  `university` bigint(20) unsigned NOT NULL,
  `departure` date NOT NULL,
  `arrive` date NOT NULL,
  `program` bigint(20) unsigned NOT NULL,
  `financingsource` bigint(20) unsigned NOT NULL,
  `diplom` varchar(200) DEFAULT NULL,
  `financingcomment` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `main` (`typenap`,`nakaz`,`student`,`faculty`,`cafedra`,`grupa`,`course`,`napriam`,`university`,`departure`,`arrive`,`program`,`financingsource`),
  KEY `nakaz` (`nakaz`),
  KEY `student` (`student`),
  KEY `faculty` (`faculty`),
  KEY `cafedra` (`cafedra`),
  KEY `grupa` (`grupa`),
  KEY `napriam` (`napriam`),
  KEY `university` (`university`),
  KEY `program` (`program`),
  KEY `financingsource` (`financingsource`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8  COLLATE=utf8_unicode_ci ;

-- --------------------------------------------------------

--
-- Table structure for table `nakaz`
--

CREATE TABLE IF NOT EXISTS `nakaz` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `number` (`number`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;

-- --------------------------------------------------------

--
-- Table structure for table `napriam`
--

CREATE TABLE IF NOT EXISTS `napriam` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8  COLLATE=utf8_unicode_ci ;

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE IF NOT EXISTS `program` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8  COLLATE=utf8_unicode_ci ;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `middlename` varchar(20) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `date` date NOT NULL,
  `contacts` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `student` (`name`,`middlename`,`lastname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8  COLLATE=utf8_unicode_ci ;

-- --------------------------------------------------------

--
-- Table structure for table `tmptable`
--

CREATE TABLE IF NOT EXISTS `tmptable` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(10) DEFAULT NULL,
  `no` varchar(10) DEFAULT NULL,
  `fullname` varchar(1024) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `middlename` varchar(200) NOT NULL,
  `faculty` varchar(1024) DEFAULT NULL,
  `course` tinyint(10) DEFAULT NULL,
  `groupa` varchar(20) DEFAULT NULL,
  `cafedra` varchar(2048) DEFAULT NULL,
  `country` varchar(1024) DEFAULT NULL,
  `city` varchar(1024) DEFAULT NULL,
  `university` varchar(1024) DEFAULT NULL,
  `departure` varchar(30) DEFAULT NULL,
  `dep` date DEFAULT NULL,
  `arr` date DEFAULT NULL,
  `arrive` varchar(30) DEFAULT NULL,
  `program` varchar(1024) DEFAULT NULL,
  `nakaz` varchar(200) DEFAULT NULL,
  `naknum` varchar(100) DEFAULT NULL,
  `nakdate` date DEFAULT NULL,
  `napriam` varchar(20) DEFAULT NULL,
  `finsource` varchar(200) DEFAULT NULL,
  `contacts` varchar(2048) DEFAULT NULL,
  `comments` varchar(2048) DEFAULT NULL,
  `fincomment` varchar(2048) DEFAULT NULL,
  `diplom` varchar(2048) DEFAULT NULL,
  `timeofstudy` varchar(1024) DEFAULT NULL,
  `filename` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8  COLLATE=utf8_unicode_ci ;

-- --------------------------------------------------------

--
-- Table structure for table `transcafedra`
--

CREATE TABLE IF NOT EXISTS `transcafedra` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cafedra` bigint(20) unsigned NOT NULL,
  `faculty` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `transcafedra` (`cafedra`,`faculty`),
  KEY `cafedra` (`cafedra`),
  KEY `faculty` (`faculty`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8  COLLATE=utf8_unicode_ci ;

-- --------------------------------------------------------

--
-- Table structure for table `university`
--

CREATE TABLE IF NOT EXISTS `university` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `country` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`,`city`,`country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8  COLLATE=utf8_unicode_ci ;

-- --------------------------------------------------------

--
-- Table structure for table `user_list`
--

CREATE TABLE IF NOT EXISTS `user_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_name` varchar(50) NOT NULL,
  `u_pass` varchar(50) DEFAULT NULL,
  `u_type` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_name` (`u_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  COLLATE=utf8_unicode_ci ;

--
-- Dumping data for table `user_list`
--

INSERT INTO `user_list` (`id`, `u_name`, `u_pass`, `u_type`) VALUES
(1, 'super', '732b14cc97a6154663ea57f83bc017ab', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `grupa`
--
ALTER TABLE `grupa`
  ADD CONSTRAINT `grupa_FK_faculty` FOREIGN KEY (`faculty`) REFERENCES `faculty` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `transcafedra`
  ADD CONSTRAINT trcaf_FK_cafedra FOREIGN KEY (cafedra) REFERENCES cafedra (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT trcaf_FK_faculty FOREIGN KEY (faculty) REFERENCES faculty (id) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `main`
  ADD CONSTRAINT main_FK_nakaz FOREIGN KEY (nakaz) REFERENCES nakaz (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT main_FK_student FOREIGN KEY (student) REFERENCES student (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT main_FK_Faculty FOREIGN KEY (faculty) REFERENCES faculty (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT main_FK_Cafedra FOREIGN KEY (cafedra) REFERENCES cafedra (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT main_FK_Grupa FOREIGN KEY (grupa) REFERENCES grupa (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT main_FK_napriam FOREIGN KEY (napriam) REFERENCES napriam (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT main_FK_university FOREIGN KEY (university) REFERENCES university (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT main_FK_program FOREIGN KEY (program) REFERENCES program (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT main_FK_financingsource FOREIGN KEY (financingsource) REFERENCES financingsource (id) ON DELETE RESTRICT ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
