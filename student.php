<?php
$dbg =0;//setup error output level
require('request.php');
require('checkcred.php');
require('conndb.php'); //connect mysql database
require('logger.php');

$newName = REQ("newName");
$newMiddlename = REQ("newMiddlename");
$newLastname = REQ("newLastname");
$newDate = REQ("newDate");
$newContacts = REQ("newContacts");
$studentId = REQ("studentId");
if (isset($_POST["Edit"])) { //Edit mode
	$query = sprintf("select name, middlename, lastname, contacts from student where id = %d", $studentId);
	if ($result = $mysqli->query($query)) {
		$row = $result->fetch_row();
		$newName = $row[0];
		$newMiddlename = $row[1];
		$newLastname = $row[2];
		$newContacts = $row[3];
	} else {
		die(sprintf("failed sql: %s",$mysqli->error));
	}
} elseif (isset($_POST["ChangeStud"])) { //UPDATE query
	if ($dbg) {	echo "DBG:update query <br>"; }
	$ChangeId=REQ('ChangeId');
	$query = sprintf("UPDATE student SET name='%s', middlename = '%s', lastname = '%s', contacts = '%s' WHERE id = '%s'",
	$newName,$newMiddlename,$newLastname,$newContacts,$ChangeId);
	unset($newName,$newMiddlename,$newLastname,$newContacts,$ChangeId);
	if ($mysqli->query($query)) {
		logg($query);
	} else {
		die(sprintf("failed update student: %s",$mysqli->error));
	}
} else { //INSERT query
	if( $newName && $newMiddlename && $newLastname ) {
		$query = sprintf("INSERT INTO student (name, middlename, lastname, date, contacts) VALUES ('%s','%s','%s','%s','%s')",$newName, $newMiddlename, $newLastname, $newDate, $newContacts);
		if ($mysqli->query($query)) {
			$studentId = $mysqli->insert_id;
			logg($query);
			$host = $_SERVER['HTTP_HOST'];
			$uri = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
			$extra = 'main_insert.php?student=' . $studentId;
			header("Location: http://$host$uri/$extra");	
			exit();
		} else {
			die(sprintf("failed insert student: %s",$mysqli->error));
		}
	} else {
		//@TODO rewrite in JS. Block insert button while any field blank
		$td1color ='';
		$td2color ='';
		$td3color ='';
		if( $newName || $newMiddlename || $newLastname ) {
			if (""==($newName)) {
				$td1color = "red";
			}
			if (""==($newMiddlename)) {
				$td2color = "red";
			}
			if (""==($newLastname)) {
				$td3color = "red";
			}
		}
	}
}
if (isset($_POST["Delete"])) { //DELETE QUERY
	if ($dbg){
		echo "DBG:delete key pressed <br>";
	}
	$query = sprintf("DELETE FROM student WHERE id = '%d'",$studentId);
	if ($mysqli->query($query)) {
		logg($query);
	}
	unset($_POST["Delete"]);
}

include("header.html"); //Print HEAD HTML
if ($dbg) {
	print "<p>Control requests: Insert|Edit|ChangeStud<br>Delete</p>
		<p>htmlVARS: newLastname newName newMiddlename newContacts ChangeId EditLine studentId</p>
		<p>Вывод тела таблицы полностю через ajax. Для изменения 2й таблици надо править studentUpdate.php</p>
	<p><b>Coments</b> parse:<br />";
	exec("grep -n '@TODO' ".$_SERVER['SCRIPT_FILENAME']." | grep -v 'grep'",$output);
	foreach ($output as $ToDo) {
		print_r($ToDo."<br>");
	}
	echo "</p>";
}
echo "
	<table>
	<tbody>
	<form method='post' action='".$_SERVER['PHP_SELF']."'>
		<tr>
			<td>Прізвище: </td>
			<td>Ім'я: </td>
			<td>По-батькові: </td>
			<td>Контакти: </td>
		</tr>
		<tr>
			<td bgcolor='".$td3color."'><input type='text' id='newLastname' name='newLastname' onkeyup='updateSearchList()' value='" . $newLastname . "' /></td>
			<td bgcolor='".$td1color."'><input type='text' id='newName' name='newName' onkeyup='updateSearchList()' value='" . $newName . "' /></td>
			<td bgcolor='".$td2color."'><input type='text' id='newMiddlename' name='newMiddlename' onkeyup='updateSearchList()' value='" . $newMiddlename . "' /></td>
			<td><input type='text' name='newContacts' value='" . $newContacts . "' /></td>
		</tr>
		<tr><td>\n";
if (isset($_REQUEST["Edit"])) {
	echo "<input type='hidden' name='ChangeId' value='" . $studentId . "' />
		<input type='submit' name='ChangeStud' value='Change' />";
} else {
	echo "<input type='submit' name='Submit' value='insert' />
		<a href='main.php'>Close</a>
		 </form></td></tr></tbody>
		<tbody id='divResult'></tbody>";
	//вывод только через ajax
	//@TODO: заменить ссылки клавишами?
}
echo "
</table><br/>
<a href='main.php'>OK</a>";
if($dbg){
	echo "
	<br/><br/>DBG: VARIBLES OUTPUT <br/>";
	phpinfo(32);
}
echo "
	
	<script type='text/javascript' src=studentUpdate.js></script>
	<script type='text/javascript'>window.onload=updateSearchList('0');</script>";
include("footer.html"); //Print FOOTER HTML
?>
