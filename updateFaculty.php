<?php
//@TODO rename to facultyUpdate.php
require('conndb.php'); //connect mysql database
require("request.php");
if (isset($_REQUEST["Cafedra"]) || isset($_REQUEST["Grupa"])) {
	$Cafedra = REQ("Cafedra");
	$Grupa = REQ("Grupa");
	if ($Cafedra) {
		$query = sprintf("SELECT S1.id, S1.name FROM faculty S1 where S1.id IN (select S2.faculty from transcafedra S2 where S2.cafedra ='%u')",$Cafedra);
	} elseif ($Grupa) {
		$query = sprintf("SELECT S1.id, S1.name FROM mobilnist_db.faculty S1 where S1.id IN (select S2.faculty from mobilnist_db.grupa S2 where S2.id ='%u')",$Grupa);
	} else {
		$query="SELECT id, name FROM faculty";
	}
	$return = $mysqli->query($query);
	if ( 0 == $return->num_rows) {
		echo "<option selected>faculty</option>
		<option value='0'>...</option>";
	}
	while ($row = $return->fetch_row()){
	echo "<option value='" . $row[0] . "'>" . $row[1] . "</option>";
	}
    $return->free();
}

if (isset($_REQUEST["fname"]) || isset($_REQUEST["page"])) {
	$FacultyName = REQ("fname");
	$page = REQ("page");
	if($FacultyName) {
		$query=sprintf("SELECT id, name FROM faculty where upper(name) like upper('%s%%') limit %u, 11",$FacultyName,$page*10);
	} else {
		$query=sprintf("SELECT id, name FROM faculty limit %u, 11",$page*10);
	}
	$return = $mysqli->query($query);
	if ($return->num_rows==11 or $page>0) {
		//@TODO check how works with 11+ lines
		switch($page) {
		case 0:
			$tableText = "<tr><td align=right><a href='javascript:updateSearchList(1);'>Next</a></td></tr>";
			break;
		default:
			$tableText = sprintf("<tr><td><a href='javascript:updateSearchList(%u);'>Back</a></td><td align=right><a href='javascript:updateSearchList(%u);'>Next</a></td></tr>",($page-1),($page+1));
		break;
		}
	} else {
		$tableText = "";
	}
	while ($row = $return->fetch_row()){
		$tableText = $tableText . "\n<tr><form id=" . $row[0] . " method='post'>
		<input type='hidden' name='id' value='" . $row[0] . "'>
		<td>".$row[1]."</td>
		<td> <input type='submit' name='Edit' value='edit' /> </td>
		<td> <input type='submit' onclick='return confirm(" . ' "Are you sure?" ' . ");' name='Delete' value='delete' /></td>
		</tr></form>\n";
	}
	echo $tableText;
	$return->free();
}
?>
